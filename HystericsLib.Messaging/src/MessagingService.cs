using System;
using System.IO;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace HystericsLib.Messaging
{
    public class MessagingService : IMessagingService
    {
        public void Send<T>(T message) => _messagingService<T>.Send(message);
        public void Subscribe<T>(Action<T> handler) => _messagingService<T>.Subscribe(handler);
        public void Unsubscribe<T>(Action<T> handler) => _messagingService<T>.Unsubscribe(handler);

        static class _messagingService<TMessage>
        {
            private static HashSet<Action<TMessage>> _handlers = new HashSet<Action<TMessage>>();

            public static void Send(TMessage message)
            {
                foreach (Action<TMessage> handler in _handlers)
                    handler(message);
            }

            public static void Subscribe(Action<TMessage> handler) => _handlers.Add(handler);
            public static void Unsubscribe(Action<TMessage> handler) => _handlers.Remove(handler);
        }
    }
}
