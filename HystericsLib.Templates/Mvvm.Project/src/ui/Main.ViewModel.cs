namespace HystericsLibMvvmTemplate.UI
{
    [ResolvesTo(typeof(MainView))]
    public class MainViewModel : ViewModel, IMainViewModel
    {
        public String Title { get; } = "HystericsLibMvvmTemplate";
		
		public Int32 Width { get; } = 1080;
		public Int32 Height { get; } = 720;
    }
}
