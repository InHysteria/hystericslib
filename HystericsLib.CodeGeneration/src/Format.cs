namespace HystericsLib.CodeGeneration.Format
{	
	public interface IFormat
	{		
		Boolean CanHandle(String filepath);
		
		T Load<T>(String filepath);
		void Save<T>(String filepath, Type generator, T graph) where T : notnull;
	}
	
	public class MultiformatSerializer : IFormat
	{
		private List<IFormat> _formats { get; }
		
		public MultiformatSerializer(IEnumerable<IFormat> formats)
		{
			_formats = formats
				.Where(s => s is not MultiformatSerializer)
				.ToList();
		}		
		
		public Boolean CanHandle(String filepath) => _formats.Any(f => f.CanHandle(filepath));
		
		public T Load<T>(String filepath) => 
			(GetSubFormat(filepath) ?? throw new Exception($"No format accepts '{filepath}'")).Load<T>(filepath);
		public void Save<T>(String filepath, Type generator, T graph)  where T : notnull => 
			(GetSubFormat(filepath) ?? throw new Exception($"No format accepts '{filepath}'")).Save<T>(filepath, generator, graph);
					
		public IFormat? GetSubFormat(String filepath) => _formats.FirstOrDefault(format => format.CanHandle(filepath));		
		
	}
}
