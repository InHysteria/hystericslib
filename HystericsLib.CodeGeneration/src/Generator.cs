namespace HystericsLib.CodeGeneration
{
	public interface IGenerator
	{
		IEnumerable<String> FilesForCompilation { get; }
		
		void Execute();
	}
	
	public abstract class Generator<TConfig> : IGenerator where TConfig : notnull
	{
		private Format.IFormat _format { get; }
		private Regex _matchRegex { get; }
		protected TaskLoggingHelper log { get; }
		protected Dictionary<String, TConfig> context { get; } = new();
		private Dictionary<String, StringBuilder> _files { get; } = new();
		
		public IEnumerable<String> FilesForCompilation => _filesForCompilation.AsEnumerable();
		private List<String> _filesForCompilation { get; } = new();
		
		protected String currentFile { get; set; } = "";
		protected virtual String name => GetType().Name;		
		
		protected Generator(TaskLoggingHelper log, Format.IFormat format) => (this.log, _format, _matchRegex) = (log, format, new Regex($"^#!\\s*{name}\\s*$", RegexOptions.Compiled));
		
		
		protected TConfig? getConfig(String path)
		{
			String absolutePath = Path.GetFullPath(Path.Combine(currentFile, path));
			return context.ContainsKey(absolutePath)
				? context[absolutePath]
				: default;				
		}
		
		protected StringBuilder createFile(String name, Boolean includeInCompilation = true) => _createFile(name, includeInCompilation);
		protected StringBuilder createFileInCurrentDirectroy(String name, Boolean includeInCompilation = true) => _createFile(Path.Combine(Path.GetDirectoryName(currentFile) ?? ".", name), includeInCompilation);
		protected StringBuilder createFileInRoot(String name, Boolean includeInCompilation = true) => _createFile(Path.GetFullPath(Path.Combine(".", name)), includeInCompilation);
		private StringBuilder _createFile(String path, Boolean includeInCompilation)
		{
			if (_files.ContainsKey(path)) 
				throw new Exception($"The file '{path}' has already been created.");
				
			if (includeInCompilation)
				_filesForCompilation.Add(path);
				
			return (_files[path] = new StringBuilder());
		}		
		
		public void Execute()
		{
			log.LogMessage($"Executing {GetType().Name} generator..");
			
			context.Clear();
			_files.Clear();
			
			//Load files..
			log.LogMessage(Microsoft.Build.Framework.MessageImportance.Low, $"Loading files..");
			foreach (String filepath in Directory
				.GetFiles(".", "*.*", SearchOption.AllDirectories)
				.Where(f => _format.CanHandle(f) && _matchRegex.IsMatch(File.ReadLines(f).First())))
			try 
			{
				log.LogMessage(Microsoft.Build.Framework.MessageImportance.Low, $"Loading '{filepath}'..");		
				context[Path.GetFullPath(filepath)] = loadConfig(filepath);
			}
			catch (Exception innerException)
			{
				throw new Exception($"Exception thrown while loading '{filepath}'", innerException);
			}
				
				
			//Parse files individually..
			beforeParse();
			foreach (KeyValuePair<String, TConfig> config in context)
			try 
			{
				log.LogMessage(Microsoft.Build.Framework.MessageImportance.Low, $"Parsing '{config.Key}'..");		
				currentFile = config.Key;
				parseConfig(config.Value);
			}
			catch (Exception innerException)
			{
				throw new Exception($"Exception thrown while parsing '{config.Key}'", innerException);
			}
				
			afterParse();
			
			//Emit files..
			foreach (KeyValuePair<String, StringBuilder> pair in _files)
			try
			{
				String directoryName = Path.GetDirectoryName(pair.Key) ?? throw new NullReferenceException($"Path.GetDirectoryName(\"{pair.Key}\") returned null.");
				if (!Directory.Exists(directoryName))
				{
					log.LogMessage(Microsoft.Build.Framework.MessageImportance.Low, $"Creating missing directory at '{directoryName}'..");		
					Directory.CreateDirectory(directoryName);
				}
					
				log.LogMessage(Microsoft.Build.Framework.MessageImportance.Low, $"Writting file '{pair.Key}'..");	
				File.WriteAllText(pair.Key, pair.Value.ToString());
			}
			catch (Exception innerException)
			{
				throw new Exception($"Exception thrown while writting '{pair.Key}'", innerException);
			}
		}						
		
		protected virtual TConfig loadConfig(String filepath)
		{
			log.LogMessage(Microsoft.Build.Framework.MessageImportance.Low, $"Deserializing object from '{filepath}'");
			return _format.Load<TConfig>(filepath);
		}
		
		protected void saveConfig(TConfig config) => saveConfig(config, currentFile);
		protected void saveConfig(TConfig config, String filepath)
		{
			log.LogMessage(Microsoft.Build.Framework.MessageImportance.Low, $"Serializing object to '{filepath}'");
 			_format.Save<TConfig>(filepath, GetType(), config);
		}
		
		protected abstract void parseConfig(TConfig config);
		protected virtual void beforeParse() {} 
		protected virtual void afterParse() {} 
	}
}