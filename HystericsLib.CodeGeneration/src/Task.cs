global using System;
global using System.Linq;
global using System.Text;
global using System.Text.RegularExpressions;
global using System.Collections.Generic;
global using Microsoft.Build.Utilities;

using System.Reflection;

using Autofac;
using Microsoft.Build.Framework;

namespace HystericsLib.CodeGeneration
{
    public class HystericsLibGenerateCodeTask : Microsoft.Build.Utilities.Task
    {		
		[Output]
		public ITaskItem[] FilesForCompilation { get; set; } = new ITaskItem[0];
		private List<ITaskItem> _filesForCompilation { get; } = new();
		
        public override Boolean Execute()
        {
            ContainerBuilder builder = new();
            
            builder.RegisterModule<CodeGenerationModule>();
            builder.RegisterInstance(Log).AsSelf().ExternallyOwned();
            
            using IContainer container = builder.Build();
            using ILifetimeScope scope = container.BeginLifetimeScope();
            
            Log.LogMessage(MessageImportance.Normal, "  Performing prebuild code generation...");          
            foreach (IGenerator generator in scope.Resolve<IEnumerable<IGenerator>>())
			{
				generator.Execute();
				_filesForCompilation.AddRange(generator
					.FilesForCompilation
					.Select(f => new TaskItem(Path.GetRelativePath(Environment.CurrentDirectory, f))));
			}
			
			FilesForCompilation = _filesForCompilation.ToArray();
            
            return !Log.HasLoggedErrors;
        }
    }
}
