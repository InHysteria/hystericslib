using Autofac;

namespace HystericsLib.CodeGeneration
{
	public class CodeGenerationModule : Module
	{			
		protected override void Load(ContainerBuilder builder)
		{			
			builder.RegisterType<Format.MultiformatSerializer>().AsSelf();
			builder.RegisterType<Format.Yaml>().AsSelf().As<Format.IFormat>();
			builder.RegisterType<Format.Json>().AsSelf().As<Format.IFormat>();
			
			builder.RegisterType<Generators.EntityFramework.EntityFrameworkGenerator>().AsSelf().As<IGenerator>();
			builder.RegisterType<Generators.Model.ModelGenerator<Generators.Model.ModelDocument, Generators.Model.Definition>>().AsSelf().As<IGenerator>();
			builder.RegisterType<Generators.Style.StyleGenerator>().AsSelf().As<IGenerator>();
			builder.RegisterType<Generators.Version.VersionGenerator>().AsSelf().As<IGenerator>();
		}		
	}
}