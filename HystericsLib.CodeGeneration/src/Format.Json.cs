using System.Text.Json;

namespace HystericsLib.CodeGeneration.Format
{
	public class Json : IFormat
	{								
		public Boolean CanHandle(String filepath) => Path.GetExtension(filepath) == ".json";
		
		public T Load<T>(String filepath) => JsonSerializer.Deserialize<T>(
			String.Join("\n",
				File.ReadLines(filepath)
					.Where(l => l.StartsWith("#")))
		) ?? throw new NullReferenceException($"Unable to parse file '{filepath}'");
		
		public void Save<T>(String filepath, Type generator, T graph) where T : notnull
		{
			if (File.Exists(filepath))
				File.Delete(filepath);
				
			File.AppendAllText(filepath, $"#!{generator.Name}\n");
			File.AppendAllText(filepath, JsonSerializer.Serialize(graph));
		}
	}
}