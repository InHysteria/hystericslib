using System.IO;

using YamlDotNet;
using YamlDotNet.Serialization;

namespace HystericsLib.CodeGeneration.Format
{
	public class Yaml : IFormat
	{				
		private ISerializer _serializer { get; } 
		private IDeserializer _deserializer { get; } 
		
		public Yaml()
		{
			_serializer = new SerializerBuilder().Build();
			_deserializer = new DeserializerBuilder().Build();
		}
		
		public Boolean CanHandle(String filepath) => Path.GetExtension(filepath) == ".yaml";
		
		public T Load<T>(String filepath) => _deserializer.Deserialize<T>(File.ReadAllText(filepath));
		public void Save<T>(String filepath, Type generator, T graph) where T : notnull
		{
			if (File.Exists(filepath))
				File.Delete(filepath);
				
			File.AppendAllText(filepath, $"#!{generator.Name}\n");
			File.AppendAllText(filepath, _serializer.Serialize(graph));
		}
	}
}