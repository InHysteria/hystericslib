namespace HystericsLib.CodeGeneration.Generators.Model
{
    public static class CoreTemplates
    {
        public const String File = 
$@"`using [[@]];|{nameof(Settings.Using)},\n`

namespace [[{nameof(Settings.Namespace)}]]
{{
`%{nameof(Enum)}|{nameof(Document<Definition>.Enums)},\n`
    
`%{nameof(Interface)}|{nameof(Document<Definition>.Interfaces)},\n`
    
`%{nameof(Class)}|{nameof(Document<Definition>.Classes)},\n`
}}";

        public const String Enum = 
$@"\tpublic enum [[Name]]
\t{{
`\t\t[[@]],|Values,\n`
\t}}";

        public const String Interface = $@"
\t`%{nameof(Accessbility)}`interface [[{nameof(Definition.Classname)}]]`%{nameof(Interfaces)}?{nameof(Definition.Interfaces)}`
\t{{
`%{nameof(InterfaceField)}|{nameof(Definition.Fields)},\n`

`%{nameof(InterfaceMethod)}|{nameof(Definition.Methods)},\n`
\t}}";

        public const String Class = $@"
\t`%{nameof(Accessbility)}`partial class [[{nameof(Definition.Classname)}]]`%{nameof(Interfaces)}?{nameof(Definition.Interfaces)}`
\t{{
`%{nameof(ClassField)}|{nameof(Definition.Fields)},\n`

`%{nameof(Constructor)}?{nameof(Definition.ConstructedFields)}`	

`%{nameof(ClassMethod)}|{nameof(Definition.Methods)},\n`
\t}}
";

        public const String Interfaces = 
$@" : `[[@]]|{nameof(Definition.Interfaces)},, `";


        public const String Constructor = 
$@"\t\tpublic [[{nameof(Definition.Classname)}]](`[[{nameof(Field.Type)}]] _[[{nameof(Field.Fieldname)}]]|{nameof(Definition.ConstructedFields)},, `)
\t\t{{
`\t\t\t[[{nameof(Field.Fieldname)}]] = _[[{nameof(Field.Fieldname)}]];|{nameof(Definition.ConstructedFields)},\n`
\t\t}}";



        public const String FieldBase = 
$@"[[{nameof(Field.Type)}]] [[{nameof(Field.Fieldname)}]] {{ get;` set;?!{nameof(Field.ReadOnly)}` }}";

        public const String InterfaceField = 
$@"\t\t`%{nameof(FieldBase)}`";

        public const String ClassField = 
$@"`\t\t\[[[@]]\]\n|{nameof(Field.Attributes)}`\t\t`%{nameof(Accessbility)}``%{nameof(FieldBase)}``%{nameof(ClassFieldDefault)}?{nameof(Field.Default)}`";

		public const String ClassFieldDefault = 
$@" = [[{nameof(Field.Default)}]];";


        public const String MethodBase = 
$@"[[{nameof(Method.ReturnType)}]] [[{nameof(Method.Methodname)}]](`%{nameof(MethodArgument)}|{nameof(Method.Arguments)},, `);";

        public const String MethodArgument = 
$@"[[{nameof(Field.Type)}]] [[{nameof(Field.Fieldname)}]]` = ?{nameof(Field.Default)}`[[{nameof(Field.Default)}]]";

        public const String InterfaceMethod = 
$@"\t\t`%{nameof(MethodBase)}`";

        public const String ClassMethod = 
$@"\t\t`%{nameof(Accessbility)}`partial `%{nameof(MethodBase)}`";

        public const String Accessbility = 
$@"[[{nameof(Field.Accessbility)}]]` ?{nameof(Field.Accessbility)}`";

    }
}
