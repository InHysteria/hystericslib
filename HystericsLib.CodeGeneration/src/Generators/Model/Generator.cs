namespace HystericsLib.CodeGeneration.Generators.Model
{
    public class ModelGenerator<TDocument, TDefinition> : Generator<TDocument> 
        where TDocument : Document<TDefinition>
        where TDefinition : Definition, new()
    {
        protected override String name { get; } = "ModelGenerator";
        
        public ModelGenerator(TaskLoggingHelper log, Format.MultiformatSerializer format) : base(log, format) { }      
        
        protected override void beforeParse()
        {
            //Make definition, enum, and field names more accessible
            foreach (TDocument document in context.Values)
            foreach (IEnumerable<KeyValuePair<String, TDefinition>> set in new[] { document.Interfaces, document.Classes })
            foreach ((String definitionName, TDefinition definition) in set)
            {
                definition.Classname = definitionName;
                foreach ((String fieldName, Field field) in definition.Fields)
                    field.Fieldname = fieldName;
                    
                foreach ((String methodName, Method method) in definition.Methods)
                {
                    method.Methodname = methodName;
                    foreach ((String fieldName, Field field) in method.Arguments)
                        field.Fieldname = fieldName;
                }
            }        
            
            //Handle imports
            foreach (TDocument recieving in context.Values.Where(x => x.GenerateFile))
                if (recieving.Import.Count > 0)
                    _handleImports(recieving, context);
        }
        
        protected override TDocument loadConfig(String filepath)
        {
            TDocument doc = base.loadConfig(filepath);
            doc.File = new FileInfo(filepath);
            return doc;
        }
        
        protected override void parseConfig(TDocument document)
        {                    
            if (!document.GenerateFile) return;
            if (document.File == null) throw new NullReferenceException("Document file was not set!");

            //Assign defaults and resolve variables
            document.Settings.Name = _replaceVariables(document.Settings.Name ?? Settings.DEFAULT_NAME, document.File, new() { document });
            document.Settings.Namespace = _replaceVariables(document.Settings.Namespace ?? Settings.DEFAULT_NAMESPACE, document.File, new() { document });
            document.Settings.File = _replaceVariables(document.Settings.File ?? Settings.DEFAULT_FILE, document.File, new() { document });
            
            foreach (IHaveTemplates templater in new[] { document.Interfaces.Values, document.Classes.Values }.OfType<IHaveTemplates>().Prepend(document))
            foreach (String template in templater.Templates.Keys)
                templater.Templates[template] = _replaceVariables(templater.Templates[template]!, document.File, new() { templater });

            foreach (IEnumerable<Definition> set in new[] { document.Interfaces.Values, document.Classes.Values })
            foreach (Definition definition in set)
            {
                foreach (Field field in definition.Fields.Values)
                {
                    field.Type = _replaceVariables(field.Type ?? Field.DEFAULT_TYPE, document.File, new() { field });
                    
                    if (document.Enums.ContainsKey(field.Type))
                        field.Default = _replaceVariables(field.Default ?? "", document.File, new() { field }) switch
                        {               
                            "" => $"({field.Type})0",                           
                            
                            String str when !str.StartsWith(field.Type + ".") => $"{field.Type}.{str}",
                            String str => str,
                        };
                    else
                        field.Default = _replaceVariables(field.Default ?? field.GetTypicalDefault(), document.File, new() { field });

					if (field.Constructed)
						definition.ConstructedFields.Add(field.Fieldname);
                }
                
                foreach (String name in definition.Interfaces)
                {
                    if (!document.Interfaces.ContainsKey(name))
                    {
                        definition.Errors.Add($"No interface defined with name '{name}'");
                        continue;
                    }
                    
                    Definition interfaceDefinition = document.Interfaces[name];
                    definition.import(interfaceDefinition);
                }

                if (definition.Implements != null)
                    definition.Interfaces.Insert(0, definition.Implements);
            }
            
            document.Settings.Using = 
                new[] 
                {
                    new[] { "System" },
                    document.Settings.Using,            
                    document.Classes.Values.SelectMany(definition => definition.Using),
                    document.Interfaces.Values.SelectMany(definition => definition.Using),
                }
                .SelectMany(x => x)
                .Distinct()
                .OrderBy(x => x)
                .ToList();              

            //Generate file
            _generate(document);
        }
        
        protected virtual void _generate(TDocument document)
        {
            if (document.Settings.File == null) return;
            
            StringBuilder writer = createFile(document.Settings.File);            
            
            writer.Append(_processTemplate(document, _getTemplate(new() { document }, nameof(CoreTemplates.File)), new() { document }));
        }

        protected virtual void _handleImports(
            TDocument recieving,
            Dictionary<String, TDocument> documents,
            HashSet<String>? processing = null)
        {
            if (recieving.File == null) throw new NullReferenceException("Document file was not set!");
            if (recieving.Import.Count <= 0) return;

            processing ??= new();
            processing.Add(recieving.File.FullName);

            foreach (String import in recieving
                .Import
                .Select(i => i.StartsWith('/')
                    ? Path.GetFullPath("." + i, Environment.CurrentDirectory)
                    : Path.GetFullPath(i, recieving?.File?.DirectoryName ?? throw new Exception("Document file is unassigned."))))
            {

                if (processing.Contains(import)) throw new Exception($"'{recieving.File.FullName}' imports '{import}' which is already requiring an import.");
                if (!documents.ContainsKey(import)) throw new Exception($"'{recieving.File.FullName}' imports '{import}' which does not exist or is not loaded by {typeof(ModelGenerator<TDocument, TDefinition>).FullName}");

                TDocument importing = documents[import];

                processing.Add(import);
                _handleImports(importing, documents, processing);

                //Settings
                recieving.Settings.Namespace ??= importing.Settings.Namespace;
                recieving.Settings.Name ??= importing.Settings.Name;
                recieving.Settings.File ??= importing.Settings.File;
                recieving.Settings.Using = new[] { recieving.Settings.Using, importing.Settings.Using }.SelectMany(x => x).ToList();
    
                //Templates
                foreach ((String templateName, String template) in importing.Templates)
					if (!recieving.Templates.ContainsKey(templateName))
						recieving.Templates[templateName] = template;                    

                //Definitions
                foreach ((Dictionary<String, TDefinition> importingDict, Dictionary<String, TDefinition> recievingDict) in new[] 
                { 
                    (importing.Interfaces, recieving.Interfaces),
                    (importing.Classes, recieving.Classes)
                })  
                foreach (KeyValuePair<String, TDefinition> importingDefinition in importingDict)
                {
                    if (!recievingDict.ContainsKey(importingDefinition.Key))
                         recievingDict[importingDefinition.Key] = new TDefinition { Classname = importingDefinition.Value.Classname };

                    Definition recievingDefinition = recievingDict[importingDefinition.Key];

                    recievingDefinition.Using = new[] { recievingDefinition.Using, importingDefinition.Value.Using }.SelectMany(x => x).ToList();
                    foreach (KeyValuePair<String, Field> importingField in importingDefinition.Value.Fields)
                    {
                        if (!recievingDefinition.Fields.ContainsKey(importingField.Key))
                             recievingDefinition.Fields[importingField.Key] = new Field { Fieldname = importingField.Value.Fieldname };

                        Field recievingField = recievingDefinition.Fields[importingField.Key];
                        recievingField.Type ??= importingField.Value.Type;
                        recievingField.Default ??= importingField.Value.Default;
						recievingField.Constructed = recievingField.Constructed | importingField.Value.Constructed;
                    }
                }
                
                //Enums
                foreach (KeyValuePair<String, HashSet<String>> importingEnum in importing.Enums)
                {
                    if (!recieving.Enums.ContainsKey(importingEnum.Key))
                         recieving.Enums[importingEnum.Key] = new();
                         
                    HashSet<String> recievingEnum = recieving.Enums[importingEnum.Key];
                    
                    foreach (String value in importingEnum.Value)
                        recievingEnum.Add(value);
                }

                processing.Remove(import);
            }

            recieving.Import.Clear();
        }


        protected virtual Regex _variableRegex { get; } = new Regex(@"\{\$([^\}]*)\}", RegexOptions.Compiled);
        protected virtual String _replaceVariables(String source, FileInfo file, List<Object> context) => _variableRegex.Replace(source, m => _getVariable(file, context, m.Groups[1].Value));
        protected virtual String _getVariable(FileInfo file, List<Object> context, String name) => _replaceVariables( //<- replace any variables introduced by the replacement.
            (context[^1], name) switch
            {
                (_, [ '.', '.', '/', .. ]) when context.Count > 1 => _getVariable(file, new(context.Take(..^1)), name.Substring(3)),
            
                (_, "AutoNamespace") => String.Join(".", file
                    ?.DirectoryName
                    ?.Split(new String[] { "/", "\\" }, StringSplitOptions.RemoveEmptyEntries)
                    ?.Skip(1)
                    ?? new[] { file?.Name ?? throw new NullReferenceException("Filename is null.") }),

                (_, "FilePath") => file.FullName,
                (_, "FileName") => Path.GetFileNameWithoutExtension(file.FullName),
                (_, "FileExtension") => file.Extension,
                (_, "DirectoryName") => file.DirectoryName,

                (TDocument document, "Name") => document.Settings.Name,
                (Definition definition, "Name") => definition.Classname,
                (Field field, "Name") => field.Fieldname,

                _ => $"!!VARIABLE '{name}' NOT FOUND!!"
            } ?? throw new NullReferenceException("Case returned null."),

            file,
            context
        );
        
        protected virtual String? _getCoreTemplate(List<Object> context, String name) => name switch
        {            
            nameof(CoreTemplates.File) => CoreTemplates.File,
            nameof(CoreTemplates.Enum) => CoreTemplates.Enum,
            nameof(CoreTemplates.Interface) => CoreTemplates.Interface,
            nameof(CoreTemplates.Class) => CoreTemplates.Class,
            nameof(CoreTemplates.Interfaces) => CoreTemplates.Interfaces,
            nameof(CoreTemplates.Constructor) => CoreTemplates.Constructor,
            nameof(CoreTemplates.FieldBase) => CoreTemplates.FieldBase,
            nameof(CoreTemplates.InterfaceField) => CoreTemplates.InterfaceField,
            nameof(CoreTemplates.ClassField) => CoreTemplates.ClassField,
            nameof(CoreTemplates.ClassFieldDefault) => CoreTemplates.ClassFieldDefault,
            nameof(CoreTemplates.MethodBase) => CoreTemplates.MethodBase,
            nameof(CoreTemplates.MethodArgument) => CoreTemplates.MethodArgument,
            nameof(CoreTemplates.InterfaceMethod) => CoreTemplates.InterfaceMethod,
            nameof(CoreTemplates.ClassMethod) => CoreTemplates.ClassMethod,
            nameof(CoreTemplates.Accessbility) => CoreTemplates.Accessbility,
            
            _ => null
        };

        private String _getTemplate(List<Object> context, String name) => name switch
        {
            [ '.', '.', '/', .. ] when context.Count > 1 => _getTemplate(new(context.Take(..^1)), name.Substring(3)),
            
            _ when context.OfType<IHaveTemplates>().ToList() is IList<IHaveTemplates> { Count: > 0 } templaters
                && templaters.LastOrDefault(t => t.Templates.ContainsKey(name)) is IHaveTemplates templater => 
                    templater.Templates[name],
                    
            _ when _getCoreTemplate(context, name) is String coreTemplate => coreTemplate,

            _ when context[^1] is IHaveTemplates => $"/* !!TEMPLATE '{name}' NOT FOUND IN '{context[^1]}' AND NO DEFAULT IS SPECIFIED!! */",
            _ => $"/* !!NO DEFAULT TEMPLATE NAMED '{name}'!!"
        };
        
        protected virtual List<Object>? _getConfigParameter(Object context, String name) => ((context, name) switch
        {
            (TDocument document, nameof(Document<Definition>.Enums)) => document.Enums.Select(e => (e.Key, e.Value.Cast<Object>().ToArray())).Cast<Object>(),
            (TDocument document, nameof(Document<Definition>.Interfaces)) => document.Interfaces.Values.Cast<Object>(),
            (TDocument document, nameof(Document<Definition>.Classes)) => document.Classes.Values.Cast<Object>(),
            (TDocument document, nameof(Settings.Using)) => document.Using.Cast<Object>(),
            (TDocument document, nameof(Settings.Namespace)) => new Object[] { document.Settings.Namespace ?? throw new NullReferenceException(nameof(Settings.Namespace) + " is null") },
            (TDocument document, nameof(Settings.Name)) => new Object[] { document.Settings.Name ?? throw new NullReferenceException(nameof(Settings.Name) + " is null") },
            (TDocument document, nameof(Settings.File)) => new Object[] { document.Settings.File ?? throw new NullReferenceException(nameof(Settings.File) + " is null") },

            (Definition definition, nameof(Definition.Classname)) => new Object[] { definition.Classname },
            (Definition definition, nameof(Definition.Using)) => definition.Using.Cast<Object>(),
            (Definition definition, nameof(Definition.Interfaces)) => definition.Interfaces.Cast<Object>(),
            (Definition definition, nameof(Definition.Fields)) => definition.Fields.Values.Cast<Object>(),
            (Definition definition, nameof(Definition.Methods)) => definition.Methods.Values.Cast<Object>(),
            (Definition definition, nameof(Definition.Accessbility)) => new Object[] { definition.Accessbility ?? "" },
            (Definition definition, nameof(Definition.ConstructedFields)) => definition.ConstructedFields
				.Where(name => definition.Fields.ContainsKey(name))
				.Select(name => definition.Fields[name])
				.Cast<Object>(),
            
            (Method method, nameof(Method.Methodname)) => new Object[] { method.Methodname },
            (Method method, nameof(Method.ReturnType)) => new Object[] { method.ReturnType },
            (Method method, nameof(Method.Arguments)) => method.Arguments.Values.Cast<Object>(),
            (Method method, nameof(Method.Accessbility)) => new Object[] { method.Accessbility ?? "" },

            (Field field, nameof(Field.Fieldname)) => new Object[] { field.Fieldname },
            (Field field, nameof(Field.Type)) => new Object[] { field.Type ?? throw new NullReferenceException(nameof(Field.Type) + " is null") },
            (Field field, nameof(Field.Default)) => new Object[] { field.Default ?? "" },
            (Field field, nameof(Field.ReadOnly)) => new Object[] { field.ReadOnly },
            (Field field, nameof(Field.Accessbility)) => new Object[] { field.Accessbility ?? "" },
            (Field field, nameof(Field.Attributes)) => field.Attributes.Cast<Object>(),

            ((String key, Object[] values), "Key" or "Name") => new Object[] { key },
            ((String key, Object[] values), "Values") => values,
            
            _ => null
        })?.ToList();
        
        private List<Object> _getParameter(List<Object> context, String name) => ((context[^1], name) switch
        {            
            (_, [ '.', '.', '/', .. ]) when context.Count > 1 => _getParameter(new(context.Take(..^1)), name.Substring(3)),
            
            (_, "!WHATAMI") => context.Reverse<Object>().Select(x => $"{x.GetType()}:{{{x}}}").Cast<Object>(),
            
            (Object head, _) when _getConfigParameter(head, name) is List<Object> result => result,

            (_, "@") => new Object[] { context[^1] },

            _ => new[] { $"/* !!NO PARAMETER '{name}' IN CONTEXT '{String.Join(", ", context.Reverse<Object>().Select(c => c.GetType()))}'!! */" }
        }).ToList();

        protected virtual Regex _parameterRegex { get; } = new Regex(@"\[\[([^\]]*)\]\]", RegexOptions.Compiled);
        protected virtual Regex _templateRegex { get; } = new Regex(options: RegexOptions.Compiled | RegexOptions.IgnorePatternWhitespace,
        pattern: @"
        `
        (
            %(?<template>[^\|`]*)\|((?<parameter>[^,?`]*),(?<join>[^`?]*)|(?<parameter>[^`?]*))
            |
            (?<source>[^\|`%]*)\|((?<parameter>[^,?`]*),(?<join>[^`?]*)|(?<parameter>[^`?]*))
            |
            %(?<template>[^`?]*)
            |
            (?<parameter>[^,?`]*),(?<join>[^`?]*)
            |
            (?<parameter>[^`?]*)
        )
        (\?(?<condition>[^`]*))?
        `");
        
        private const String LEFT_BRACE_ESCAPE = "»æ»";
        private const String RIGHT_BRACE_ESCAPE = "«æ«";
        protected virtual String _processTemplate(TDocument document, String template, List<Object> context) =>
            _parameterRegex.Replace(
            _templateRegex.Replace(
                template.Replace("\\[", LEFT_BRACE_ESCAPE).Replace("\\]", RIGHT_BRACE_ESCAPE).Trim('\n'),
                m => (m.Groups["template"], m.Groups["source"], m.Groups["parameter"], m.Groups["condition"]) switch
                {
                    (_,_,_, { Success: true, Value: String condition }) when condition.StartsWith("!") &&
                        _getParameter(context, condition.TrimStart('!')) is not { Count: 0 } and not [ false or null or "" ] => "",
                        
                    (_,_,_, { Success: true, Value: String condition }) when !condition.StartsWith("!") &&
                        _getParameter(context, condition) is { Count: 0 } or [ false or null or "" ] => "",
                    
                    ({ Success: true, Value: String template }, 
                     _, 
                     { Success: true, Value: String parameter }, 
                     _) =>
                        String.Join(m.Groups["join"].Value,
                            _getParameter(context, parameter)
                                .Select(o => _processTemplate(document, _getTemplate(new(context) { o }, template), new(context) { o }))
                        ),
                        
                    (_,
                     { Success: true, Value: String source }, 
                     { Success: true, Value: String parameter }, 
                     _) =>
                        String.Join(m.Groups["join"].Value,
                            _getParameter(context, parameter)
                                .Select(o => _processTemplate(document, source, new(context) { o }))
                        ),
                        
                    ({ Success: true, Value: String template },
                     _,
                     _,
                     _) =>
                        _processTemplate(document, _getTemplate(context, template), context),
                    
                    (_, 
                     _, 
                     { Success: true, Value: String parameter }, 
                     _) => 
                        _parameterRegex.Replace(parameter, _ => String.Join(m.Groups["join"].Value, _getParameter(context, parameter).OfType<String>())),
                        
                    _ => $"<CANNOT PARSE {m.Groups["template"].Value}:{m.Groups["source"].Value}:{m.Groups["parameter"].Value}>"//m.Value
                }),

                m => String.Join("", _getParameter(context, m.Groups[1].Value).OfType<String>()))

            .Replace("\\n", "\n")
            .Replace("\\t", "\t")
            .Replace(LEFT_BRACE_ESCAPE, "[")
            .Replace(RIGHT_BRACE_ESCAPE, "]");
    }
}