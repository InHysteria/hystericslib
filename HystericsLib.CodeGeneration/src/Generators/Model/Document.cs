namespace HystericsLib.CodeGeneration.Generators.Model
{
    public interface IHaveTemplates
    {
        Dictionary<String, String> Templates { get; }
    }
    
    public interface IHaveArbitraryData
    {
        Dictionary<String, String> Data { get; }
    }
    
    public class ModelDocument : Document<Definition> { }
    public class Document<TDefinition> : IHaveArbitraryData, IHaveTemplates where TDefinition : Definition, new()
    {        
        [YamlDotNet.Serialization.YamlIgnore]
        [System.Text.Json.Serialization.JsonIgnore]
        public FileInfo? File { get; set; } = default;
        public Boolean GenerateFile { get; set; } = true;

        public Settings Settings { get; set; } = new();
        public List<String> Import { get; set; } = new();
        public Dictionary<String, TDefinition> Interfaces { get; set; } = new();
        public Dictionary<String, TDefinition> Classes { get; set; } = new();
        public Dictionary<String, String> Templates { get; set; } = new();
        public Dictionary<String, HashSet<String>> Enums { get; set; } = new();
            
        public Dictionary<String, String> Data { get; } = new();

        public IEnumerable<String> Using => Classes
            .Values
            .SelectMany(d => d.Using)
            .Concat(Settings.Using)
            .Distinct()
            .OrderBy(x => x.Length);
    }

    public class Settings : IHaveArbitraryData
    {
        public const String DEFAULT_NAMESPACE = "{$AutoNamespace}";
        public const String DEFAULT_NAME = "{$FileName}";
        public const String DEFAULT_FILE = "{$DirectoryName}/.{$Name}.cs";

        public String? Namespace { get; set; } = null;
        public String? Name { get; set; } = null;
        public String? File { get; set; } = null;
        public List<String> Using { get; set; } = new();
            
        public Dictionary<String, String> Data { get; } = new();
    }

    public class Definition : IHaveArbitraryData, IHaveTemplates
    {
        [YamlDotNet.Serialization.YamlIgnore]
        [System.Text.Json.Serialization.JsonIgnore]
        public String Classname { get; set; } = "";
        public List<String> Using { get; set; } = new();    
        public String? Implements { get; set; } = null;
        public List<String> Interfaces { get; set; } = new();
        public Dictionary<String, Field> Fields { get; set; } = new();  
        public Dictionary<String, Method> Methods { get; set; } = new();        
        public Dictionary<String, String> Templates { get; set; } = new();           
        public String? Accessbility { get; set; } = "public";
        
        [YamlDotNet.Serialization.YamlIgnore]
        [System.Text.Json.Serialization.JsonIgnore]
        public List<String> Errors { get; set; } = new();
        
        [YamlDotNet.Serialization.YamlIgnore]
        [System.Text.Json.Serialization.JsonIgnore]
        public HashSet<String> ConstructedFields { get; set; } = new();
            
        public Dictionary<String, String> Data { get; } = new();        
                
        public virtual void import(Definition other)
        {                    
            Implements ??= other.Implements;

            foreach ((String fieldName, Field field) in other.Fields)
                Fields[fieldName] = field;
            
            foreach ((String methodName, Method method) in other.Methods)
                Methods[methodName] = method;
                
            foreach ((String templateName, String template) in other.Templates)
                if (!Templates.ContainsKey(templateName))
                    Templates[template] = template;

			foreach (String field in other.ConstructedFields)
				ConstructedFields.Add(field);
        }
    }

    public class Field : IHaveArbitraryData
    {
        public const String DEFAULT_TYPE = "Object";

        [YamlDotNet.Serialization.YamlIgnore]
        [System.Text.Json.Serialization.JsonIgnore]
        public String Fieldname { get; set; } = "";
        public String? Type { get; set; } = null;
        public String? Default { get; set; } = null;
        public Boolean ReadOnly { get; set; } = false;       
        public Boolean Constructed { get; set; } = false;       
        public String? Accessbility { get; set; } = "public";
        public List<String> Attributes { get; set; } = new();
            
        public Dictionary<String, String> Data { get; } = new();

        public String GetTypicalDefault() => this switch
        {   
			{ Constructed: true } => "",

            { Type: [ .., '?' ] } => "null",
            { Type: "String" or "string" } => "\"\"",            
            { Type: "Byte" or "byte" } => "0",            
            { Type: "Int32" or "int" } => "0",            
            { Type: "Int64" or "long" } => "0",            
            { Type: "Double" or "double" } => "0.0",            
            { Type: "Single" or "float" } => "0.0f",            
            { Type: "Char" or "char" } => "''",            
            { Type: "Boolean" or "bool" } => "false",
            
            null => throw new NullReferenceException($"{nameof(GetTypicalDefault)} must be ran after Type has been set."),
            _ => $"new {Type}()"
        };
    }

    public class Method : IHaveArbitraryData
    {
        [YamlDotNet.Serialization.YamlIgnore]
        [System.Text.Json.Serialization.JsonIgnore]
        public String Methodname { get; set; } = "";
        public String ReturnType { get; set; } = "Object";        
        public Dictionary<String, Field> Arguments { get; set; } = new();
        public String? Accessbility { get; set; } = "public";
            
        public Dictionary<String, String> Data { get; } = new();
    }
}
