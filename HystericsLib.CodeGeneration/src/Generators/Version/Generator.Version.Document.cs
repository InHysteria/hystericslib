namespace HystericsLib.CodeGeneration.Generators.Version
{
    public class VersionGeneratorDocument
    {
        public String Format = "1.{Major}.{Minor}.{Build:1}";
        
        public Dictionary<String, VersionGeneratorDocumentVariable> Variables { get; set; } = new();
        public VersionGeneratorDocumentOutput Outputs { get; set; } = new();
    }
    
    public class VersionGeneratorDocumentVariable
    {
        public Int32 Value = 0;
        public Boolean AutoIncrement = false;
    }
    
    public class VersionGeneratorDocumentOutput
    {
        public VersionGeneratorDocumentOutputClass? Class { get; set; } = null;
        public VersionGeneratorDocumentOutputFile? File { get; set; } = null;
    }
    
    public class VersionGeneratorDocumentOutputClass
    {
        public String Filename = "Version.cs";
        public String Classname = "Version";
        public String Namespace = "No_Namespace_Specified";
    }
    
    public class VersionGeneratorDocumentOutputFile
    {
        public String Filename = "Version.txt";
    }
}
