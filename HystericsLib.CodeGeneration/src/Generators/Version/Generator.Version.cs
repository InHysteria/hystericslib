namespace HystericsLib.CodeGeneration.Generators.Version
{
    public partial class VersionGenerator : Generator<VersionGeneratorDocument>
    {		
		public VersionGenerator(TaskLoggingHelper log, Format.MultiformatSerializer format) : base(log, format) { }        

        private static Regex _versionRegex = new Regex(@"\{((?<var>[^:\}]+)|(?<var>[^:\}]+):(?<auto>\d+))\}", RegexOptions.Compiled);
        protected override void parseConfig(VersionGeneratorDocument document)
        {   
            foreach (VersionGeneratorDocumentVariable variable in document.Variables.Values.Where(v => v.AutoIncrement))
                variable.Value++;            
            
            String version = _versionRegex.Replace(document.Format, m =>
            {                
                String variable =  m.Groups["var"].Value;
                if (!document.Variables.ContainsKey(variable))
                    document.Variables[variable] = new();
                    
                return document.Variables[variable].Value.ToString();
            });    
            
            if (document.Outputs.Class is VersionGeneratorDocumentOutputClass classDocument)
            {
                StringBuilder writer = createFile(classDocument.Filename);

                writer.AppendLine($"namespace {classDocument.Namespace}");
                writer.AppendLine( "{");
                writer.AppendLine($"\tpublic static class {classDocument.Classname}");
                writer.AppendLine( "\t{");
                writer.AppendLine($"\t\tpublic const System.String VersionNumber = \"{version}\";");
                writer.AppendLine( "\t}");
                writer.AppendLine( "}");
            }  
            
            if (document.Outputs.File is VersionGeneratorDocumentOutputFile fileDocument)
            {
                StringBuilder writer = createFile(fileDocument.Filename);

                writer.AppendLine(version);
            }
            
            saveConfig(document);
        }
    }
}
