using HystericsLib.CodeGeneration.Generators.Model;

namespace HystericsLib.CodeGeneration.Generators.EntityFramework
{
    public class EntityFrameworkDocument : Document<EntityFrameworkDefinition>
    {        
        public Database Database { get; set; } = new();
        public EntityFrameworkSettings EntityFrameworkSettings { get; set; } = new();
    }
    
    public class EntityFrameworkSettings
    {
        public List<String> GeneratedRelationshipFieldAttributes { get; set; } = new();
    }
    
    public class Database
    {
        public String? Name { get; set; }
        public String? Interface { get; set; }
    }
    
    public class EntityFrameworkDefinition : Definition
    {
        public String? PrimaryKey { get; set; }
        public String? Tablename { get; set; }
        
        public List<Relationship> Relationships { get; set; } = new();
        public List<String> Configuration { get; set; } = new();
        
        public override void import(Definition other)
        {   
            base.import(other);            
            
            if (other is not EntityFrameworkDefinition otherEf)
                return;
                
            PrimaryKey ??= otherEf.PrimaryKey;
                
            foreach (Relationship relationship in otherEf.Relationships)
                if (!Relationships.Any(r => (r.HasOne ?? r.HasMany) == (relationship.HasOne ?? relationship.HasMany)))
                    Relationships.Add(relationship);
                
            foreach (String configuration in otherEf.Configuration)
                Configuration.Add(configuration);
        }
    }
    
    public class Relationship 
    {         
        public String? HasMany { get; set; }
        public String? HasOne { get; set; }
        
        public String? WithMany { get; set; }
        public String? WithOne { get; set; }         
        
        public String? In { get; set; }
        
        public Boolean IsRequired { get; set; } = false;
        
        public override String ToString() => String.Join("", new String[]
        {
            this switch
            {
                { HasMany: String field } => $".HasMany(e => e.{field})",
                { HasOne: String field } => $".HasOne(e => e.{field})",
                _ => throw new Exception($"One of [\"{nameof(HasMany)}\", \"{nameof(HasOne)}\"] must be set.")
            },
            this switch
            {
                { WithMany: "" } => $".WithMany()",
                { WithMany: String field } => $".WithMany(e => e.{field})",                
                { WithOne: "" } => $".WithOne()",
                { WithOne: String field } => $".WithOne(e => e.{field})",
                _ => throw new Exception($"One of [\"{nameof(WithMany)}\", \"{nameof(WithOne)}\"] must be set.")
            },
           
            IsRequired
                ? ".IsRequired()"
                : ".IsRequired(false)"
           
        }.Where(x => x != String.Empty));        
    }
}
