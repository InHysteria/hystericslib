using HystericsLib.CodeGeneration.Generators.Model;

using BaseTemplate = HystericsLib.CodeGeneration.Generators.Model.CoreTemplates;

namespace HystericsLib.CodeGeneration.Generators.EntityFramework
{
    public static class CoreTemplates
    {
        public const String File = 
$@"`using [[@]];|{nameof(Settings.Using)},\n`

using Microsoft.EntityFrameworkCore;

namespace [[{nameof(Settings.Namespace)}]]
{{
`%{nameof(BaseTemplate.Enum)}|{nameof(Document<Definition>.Enums)},\n`

`%{nameof(DatabaseInterface)}`

`%{nameof(Database)}`
    
`%{nameof(BaseTemplate.Interface)}|{nameof(Document<Definition>.Interfaces)},\n`
    
`%{nameof(BaseTemplate.Class)}|{nameof(Document<Definition>.Classes)},\n`
}}";

        public const String DatabaseInterface = 
$@"\tpublic partial interface `[[Interface]]|{nameof(EntityFrameworkDocument.Database)}`
\t{{
 `\t\tDbSet<[[Classname]]> [[Tablename]] {{ get; }}|{nameof(Document<Definition>.Classes)},\n`
\t}}";

        public const String Database = 
$@"\tpublic partial class `[[Name]]|{nameof(EntityFrameworkDocument.Database)}` : DbContext, `[[Interface]]|{nameof(EntityFrameworkDocument.Database)}`
\t{{
`\t\tpublic DbSet<[[Classname]]> [[Tablename]] {{ get; }}|{nameof(Document<Definition>.Classes)},\n`
                
\t\tpublic `[[Name]]|{nameof(EntityFrameworkDocument.Database)}`(DbContextOptions<`[[Name]]|{nameof(EntityFrameworkDocument.Database)}`> options) : base(options)
\t\t{{
`\t\t\t[[Tablename]] = Set<[[Classname]]>();|{nameof(Document<Definition>.Classes)},\n`        
\t\t}}

\t\tprotected override void OnModelCreating(ModelBuilder conf)
\t\t{{
`%{nameof(ClassConfiguration)}|{nameof(Document<Definition>.Classes)},\n\n`
\t\t}}
\t}}";

/*                              
\t\tpublic void EnsureCreatedAndLoad()
\t\t{{
\t\t\tDatabase.EnsureCreated();
`\t\t\t[[Tablename]].Load();|{nameof(Document<Definition>.Classes)},\n`
\t\t}}
*/

        public const String ClassConfiguration = 
$@"`\t\t\tconf.Entity<[[../{nameof(EntityFrameworkDefinition.Classname)}]]>().[[@]];|{nameof(EntityFrameworkDefinition.Configuration)},\n`
`\t\t\tconf.Entity<[[../{nameof(EntityFrameworkDefinition.Classname)}]]>()[[@]];|{nameof(EntityFrameworkDefinition.Relationships)},\n`";
    }
}
