using HystericsLib.CodeGeneration.Generators.Model;

namespace HystericsLib.CodeGeneration.Generators.EntityFramework
{
	public class EntityFrameworkGenerator : ModelGenerator<EntityFrameworkDocument, EntityFrameworkDefinition>
	{
        protected override String name { get; } = nameof(EntityFrameworkGenerator);
		
        public EntityFrameworkGenerator(TaskLoggingHelper log, Format.MultiformatSerializer format) : base(log, format) { }
		
		protected override void _generate(EntityFrameworkDocument document)
		{
			_buildRelations(document);
			
			base._generate(document);
		}
		
		private void _buildRelations(EntityFrameworkDocument document)
		{						
			Dictionary<String, EntityFrameworkDefinition> AllDefinitions = new[]
			{
				document.Interfaces,
				document.Classes,
			}
				.SelectMany(x => x)
				.GroupBy(x => x.Key)
				.ToDictionary(x => x.Key, x => x.First().Value);
			
			foreach ((Relationship relationship, EntityFrameworkDefinition left, EntityFrameworkDefinition right) in AllDefinitions
				.Values
				.SelectMany(c => c.Relationships.Select(r => (relation: r, left: c, right: r.In ))
				.Select(t => t.relation is
					{ In: not null } and
					({ HasMany: not null } or { HasOne: not null }) and
					({ WithMany: not null } or { WithOne: not null }) &&
					t.right is not null &&
					AllDefinitions.ContainsKey(t.right)									
						? (	t.relation,
							t.left,
							AllDefinitions[t.right])
												
						: throw new FormatException("Relationship specific is invalid, both sides of relationship must be set and it must refer to another class/interface which is defined in the model through the 'In' property."))))
			{
				Field leftField = new() 
				{
					Fieldname = relationship.HasMany ?? relationship.HasOne ?? throw new NullReferenceException(),
					Type = relationship switch
					{
						{ HasMany: String field } => $"List<{right.Classname}>",
						{ HasOne: String field, IsRequired: true } => $"{right.Classname}",
						{ HasOne: String field, IsRequired: false } => $"{right.Classname}?",
						
						_ => throw new Exception()
					},
					Attributes = document.EntityFrameworkSettings.GeneratedRelationshipFieldAttributes,
					Accessbility = "public virtual"
				};
				
				Field rightField = new() 
				{
					Fieldname = relationship.WithMany ?? relationship.WithOne ?? throw new NullReferenceException(),
					Type = relationship switch
					{
						{ WithMany: String field } => $"List<{left.Classname}>",
						{ WithOne: String field, IsRequired: true } => $"{left.Classname}",
						{ WithOne: String field, IsRequired: false } => $"{left.Classname}?",
						
						_ => throw new Exception()
					},
					Attributes = document.EntityFrameworkSettings.GeneratedRelationshipFieldAttributes,
					Accessbility = "public virtual"
				};
				
				leftField.Default = leftField.GetTypicalDefault();
				rightField.Default = rightField.GetTypicalDefault();
				
				if (!left.Fields.ContainsKey(leftField.Fieldname) ||
					 left.Fields[leftField.Fieldname].Type != leftField.Type)
				  	 left.Fields[leftField.Fieldname] = leftField;
				
				if (rightField.Fieldname != "" && 
					(!right.Fields.ContainsKey(rightField.Fieldname) ||
					  right.Fields[rightField.Fieldname].Type != rightField.Type))
				  	 right.Fields[rightField.Fieldname] = rightField;
			}
		}
		
		protected override String? _getCoreTemplate(List<Object> context, String name) => name switch
		{
			nameof(CoreTemplates.File) => CoreTemplates.File,
			nameof(CoreTemplates.DatabaseInterface) => CoreTemplates.DatabaseInterface,
			nameof(CoreTemplates.Database) => CoreTemplates.Database,
			nameof(CoreTemplates.ClassConfiguration) => CoreTemplates.ClassConfiguration,
			
			_ => base._getCoreTemplate(context, name)
		};

		protected override List<Object>? _getConfigParameter(Object context, String name) => ((context, name) switch
		{
			(EntityFrameworkDocument document, nameof(EntityFrameworkDocument.Database)) => new(){ document.Database },
			(Database database, nameof(Database.Name)) => new(){ database.Name ?? "ModelDbContext" },
			(Database database, nameof(Database.Interface)) => new(){ database.Interface ?? "IDatabase" },
			(EntityFrameworkDefinition definition, nameof(EntityFrameworkDefinition.PrimaryKey)) => new(){ definition.PrimaryKey ?? "ID" },
			(EntityFrameworkDefinition definition, nameof(EntityFrameworkDefinition.Tablename)) => new(){ definition.Tablename ?? definition.Classname + "Table" },
			(EntityFrameworkDefinition definition, nameof(EntityFrameworkDefinition.Configuration)) => definition.Configuration.Cast<Object>().ToList(),
			(EntityFrameworkDefinition definition, nameof(EntityFrameworkDefinition.Relationships)) => definition.Relationships.Cast<Object>().ToList(),
			(Relationship relationship, nameof(Relationship.HasMany)) => new(){ relationship.HasMany ?? "" },
			(Relationship relationship, nameof(Relationship.HasOne)) => new(){ relationship.HasOne ?? "" },
			(Relationship relationship, nameof(Relationship.WithMany)) => new(){ relationship.WithMany ?? "" },
			(Relationship relationship, nameof(Relationship.WithOne)) => new(){ relationship.WithOne ?? "" },
			(Relationship relationship, nameof(Relationship.IsRequired)) => new(){ relationship.IsRequired },
			(Relationship relationship, nameof(Relationship.ToString) or "@")  => new(){ relationship.ToString() },
			
			_ => base._getConfigParameter(context, name)
		});
	}
}
