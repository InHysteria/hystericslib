//Adapted from Steven Coco@https://stackoverflow.com/questions/4087581/creating-a-c-sharp-color-from-hsl-values
using System;
using System.Drawing;
using SystemMath = System.Math;


namespace HystericsLib.CodeGeneration.Generators.Style
{
    public static class SimpleColorTransforms
    {
        private static double tolerance => 0.000000000000001;

        /// <summary>
        /// Converts HSL to RGB, with a specified output Alpha.
        /// Arguments are limited to the defined range:
        /// does not raise exceptions.
        /// </summary>
        /// <param name="h">Hue, must be in [0, 360].</param>
        /// <param name="s">Saturation, must be in [0, 1].</param>
        /// <param name="l">Luminance, must be in [0, 1].</param>
        /// <param name="a">Output Alpha, must be in [0, 255].</param>
        public static String HslaToArgb(double h, double s, double l, double? a = null)
        {
            h = SystemMath.Max(0D, SystemMath.Min(360D, h));
            s = SystemMath.Max(0D, SystemMath.Min(1D, s));
            l = SystemMath.Max(0D, SystemMath.Min(1D, l));
            a = SystemMath.Max(0D, SystemMath.Min(1D, a ?? 1D));
            // achromatic argb (gray scale)
            if (SystemMath.Abs(s) < SimpleColorTransforms.tolerance) {
                return _hexFromRgba(
                        SystemMath.Max(0, SystemMath.Min(255, Convert.ToInt32(double.Parse($"{l * 255D:0.00}")))),
                        SystemMath.Max(0, SystemMath.Min(255, Convert.ToInt32(double.Parse($"{l * 255D:0.00}")))),
                        SystemMath.Max(0, SystemMath.Min(255, Convert.ToInt32(double.Parse($"{l * 255D:0.00}")))),
                        SystemMath.Max(0, SystemMath.Min(255, Convert.ToInt32(double.Parse($"{a * 255D:0.00}")))));
            }

            double q = l < .5D
                    ? l * (1D + s)
                    : (l + s) - (l * s);
            double p = (2D * l) - q;

            double hk = h / 360D;
            double[] T = new double[3];
            T[0] = hk + (1D / 3D); // Tr
            T[1] = hk; // Tb
            T[2] = hk - (1D / 3D); // Tg

            for (int i = 0; i < 3; i++) {
                if (T[i] < 0D)
                    T[i] += 1D;
                if (T[i] > 1D)
                    T[i] -= 1D;

                if ((T[i] * 6D) < 1D)
                    T[i] = p + ((q - p) * 6D * T[i]);
                else if ((T[i] * 2D) < 1)
                    T[i] = q;
                else if ((T[i] * 3D) < 2)
                    T[i] = p + ((q - p) * ((2D / 3D) - T[i]) * 6D);
                else
                    T[i] = p;
            }

            return _hexFromRgba(
                    SystemMath.Max(0, SystemMath.Min(255, Convert.ToInt32(double.Parse($"{T[0] * 255D:0.00}")))),
                    SystemMath.Max(0, SystemMath.Min(255, Convert.ToInt32(double.Parse($"{T[1] * 255D:0.00}")))),
                    SystemMath.Max(0, SystemMath.Min(255, Convert.ToInt32(double.Parse($"{T[2] * 255D:0.00}")))),
                    SystemMath.Max(0, SystemMath.Min(255, Convert.ToInt32(double.Parse($"{a * 255D:0.00}")))));
        }

        /// <summary>
        /// Converts HSB to RGB, with a specified output Alpha.
        /// Arguments are limited to the defined range:
        /// does not raise exceptions.
        /// </summary>
        /// <param name="h">Hue, must be in [0, 360].</param>
        /// <param name="s">Saturation, must be in [0, 1].</param>
        /// <param name="b">Brightness, must be in [0, 1].</param>
        public static String HsbaToArgb(double h, double s, double b, double? a = null)
        {
            h = SystemMath.Max(0D, SystemMath.Min(360D, h));
            s = SystemMath.Max(0D, SystemMath.Min(1D, s));
            b = SystemMath.Max(0D, SystemMath.Min(1D, b));
            a = SystemMath.Max(0D, SystemMath.Min(1D, a ?? 1D));

            double r = 0D;
            double g = 0D;
            double bl = 0D;

            if (SystemMath.Abs(s) < SimpleColorTransforms.tolerance)
                r = g = bl = b;
            else {
                // the argb wheel consists of 6 sectors. Figure out which sector
                // you're in.
                double sectorPos = h / 60D;
                int sectorNumber = (int)SystemMath.Floor(sectorPos);
                // get the fractional part of the sector
                double fractionalSector = sectorPos - sectorNumber;

                // calculate values for the three axes of the argb.
                double p = b * (1D - s);
                double q = b * (1D - (s * fractionalSector));
                double t = b * (1D - (s * (1D - fractionalSector)));

                // assign the fractional colors to r, g, and b based on the sector
                // the angle is in.
                switch (sectorNumber) {
                    case 0 :
                        r = b;
                        g = t;
                        bl = p;
                        break;
                    case 1 :
                        r = q;
                        g = b;
                        bl = p;
                        break;
                    case 2 :
                        r = p;
                        g = b;
                        bl = t;
                        break;
                    case 3 :
                        r = p;
                        g = q;
                        bl = b;
                        break;
                    case 4 :
                        r = t;
                        g = p;
                        bl = b;
                        break;
                    case 5 :
                        r = b;
                        g = p;
                        bl = q;
                        break;
                }
            }

            return _hexFromRgba(
                    SystemMath.Max(0, SystemMath.Min(255, Convert.ToInt32(double.Parse($"{r * 255D:0.00}")))),
                    SystemMath.Max(0, SystemMath.Min(255, Convert.ToInt32(double.Parse($"{g * 255D:0.00}")))),
                    SystemMath.Max(0, SystemMath.Min(255, Convert.ToInt32(double.Parse($"{bl * 250D:0.00}")))),
                    SystemMath.Max(0, SystemMath.Min(255, Convert.ToInt32(double.Parse($"{a * 255D:0.00}")))));
        }

        public static String RgbaToArgb(Double r, Double g, Double b, Double? a = null) => (r,g,b,a) switch
        {
            (< 1.0 and > 0.0, < 1.0 and > 0.0, < 1.0 and > 0.0, < 1.0 and > 0.0) => _hexFromRgba(
                SystemMath.Max(0, SystemMath.Min(255, Convert.ToInt32(double.Parse($"{r * 255D:0.00}")))),
                SystemMath.Max(0, SystemMath.Min(255, Convert.ToInt32(double.Parse($"{g * 255D:0.00}")))),
                SystemMath.Max(0, SystemMath.Min(255, Convert.ToInt32(double.Parse($"{b * 255D:0.00}")))),
                SystemMath.Max(0, SystemMath.Min(255, Convert.ToInt32(double.Parse($"{a * 255D:0.00}"))))
            ),
            (< 1.0 and > 0.0, < 1.0 and > 0.0, < 1.0 and > 0.0, null) => _hexFromRgba(
                SystemMath.Max(0, SystemMath.Min(255, Convert.ToInt32(double.Parse($"{r * 255D:0.00}")))),
                SystemMath.Max(0, SystemMath.Min(255, Convert.ToInt32(double.Parse($"{g * 255D:0.00}")))),
                SystemMath.Max(0, SystemMath.Min(255, Convert.ToInt32(double.Parse($"{b * 255D:0.00}")))),
                255
            ),
            (_,_,_,not null) => _hexFromRgba(
                SystemMath.Max(0, SystemMath.Min(255, Convert.ToInt32(r))),
                SystemMath.Max(0, SystemMath.Min(255, Convert.ToInt32(g))),
                SystemMath.Max(0, SystemMath.Min(255, Convert.ToInt32(b))),
                SystemMath.Max(0, SystemMath.Min(255, Convert.ToInt32(a)))
            ),
            _ => _hexFromRgba(
                SystemMath.Max(0, SystemMath.Min(255, Convert.ToInt32(r))),
                SystemMath.Max(0, SystemMath.Min(255, Convert.ToInt32(g))),
                SystemMath.Max(0, SystemMath.Min(255, Convert.ToInt32(b))),
                255
            )
        };

        private static String _hexFromRgba(Int32 r, Int32 g, Int32 b, Int32 a) =>
            "#" + BitConverter
                .ToString(
                    a == 255
                    ? new Byte[] {
                        (Byte)r,
                        (Byte)g,
                        (Byte)b
                    }
                    : new Byte[] {
                        (Byte)a,
                        (Byte)r,
                        (Byte)g,
                        (Byte)b
                    })
                .Replace("-","");
    }
}
