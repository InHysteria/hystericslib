using System;
using System.Linq;
using System.Collections.Generic;

namespace HystericsLib.CodeGeneration.Generators.Style
{
    public class StyleGeneratorDocument
    {
        public String? Filepath = null;
        public String Name = "";
        public String Namespace = "";

        public Dictionary<String, String> Namespaces = new();
        public Dictionary<String, String> Constants = new();
        public Dictionary<String, Dictionary<String, String>> Resources = new();
        public Dictionary<String, Object> Styles = new();
    }

    public static class StyleGeneratorDocumentExtensions
    {
        public static IEnumerable<StyleGeneratorDocument> Merge(this IEnumerable<StyleGeneratorDocument> documents) => documents
            .GroupBy(x => (Name: x.Name, Namespace: x.Namespace))
            .Select(g => new StyleGeneratorDocument
            {
                Filepath = g.First().Filepath,
                Name = g.Key.Name,
                Namespace = g.Key.Namespace,

                Namespaces = g
                    .SelectMany(d => d.Namespaces)
                    .GroupBy(p => p.Key)
                    .Select(g => g.Last())
                    .ToDictionary(p => p.Key, p => p.Value),
                Constants = g
                    .SelectMany(d => d.Constants)
                    .GroupBy(p => p.Key)
                    .Select(g => g.Last())
                    .ToDictionary(p => p.Key, p => p.Value),

                Resources = g
                    .SelectMany(d => d.Resources)
                    .GroupBy(p => p.Key)
                    .Select(g => (
                        Key: g.Key,
                        Value: g
                            .SelectMany(p => p.Value)
                            .GroupBy(p => p.Key)
                            .Select(sg => sg.Last())
                            .ToDictionary(p => p.Key, p => p.Value)
                    ))
                    .ToDictionary(p => p.Key, p => p.Value),
                Styles = g
                    .SelectMany(d => d.Styles)
                    .GroupBy(p => p.Key)
                    .Select(g => g.Last())
                    .ToDictionary(p => p.Key, p => p.Value),
            });
    }
}
