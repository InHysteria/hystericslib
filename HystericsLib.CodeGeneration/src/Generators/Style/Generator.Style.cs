namespace HystericsLib.CodeGeneration.Generators.Style
{
    public class StyleGenerator : Generator<StyleGeneratorDocument>
    {
        public StyleGenerator(TaskLoggingHelper log, Format.MultiformatSerializer format) : base(log, format) { }      
                
        protected override StyleGeneratorDocument loadConfig(String filepath)
        {
            StyleGeneratorDocument doc = base.loadConfig(filepath);
            doc.Filepath = filepath;
            return doc;
        }		
		
        protected override void parseConfig(StyleGeneratorDocument document)
        {
            if (document.Filepath == null) return;
            
            StringBuilder writer;
			
			String pathRoot = "." + Path.GetFileNameWithoutExtension(document.Filepath);
            
            writer = createFileInCurrentDirectroy($"{pathRoot}.xaml", includeInCompilation: false);
            writer.AppendLine( "<Styles xmlns=\"https://github.com/avaloniaui\"");
            writer.AppendLine( "        xmlns:x=\"http://schemas.microsoft.com/winfx/2006/xaml\"");
            writer.AppendLine( "        xmlns:sys=\"clr-namespace:System;assembly=netstandard\"");
            foreach (KeyValuePair<String, String> ns in document.Namespaces)
                writer.AppendLine($"        xmlns:{ns.Key}=\"{ns.Value}\"");
            writer.AppendLine($"        x:Class=\"{document.Namespace}.{document.Name}\">");
            writer.AppendLine( "\t<Styles.Resources>");
            foreach (KeyValuePair<String, Dictionary<String, String>> resource in document.Resources)
            {
                if (!resource.Value.ContainsKey("_type"))
                {
                    writer.AppendLine($"<!-- Resources.{resource.Key} is missing a _type entry. -->");
                    continue;
                }

                String type = resource.Value["_type"];
                List<String> attributes = new List<String> { $"x:Key=\"{resource.Key}\"" };
                List<String> inner = new();

                if (resource.Value.ContainsKey("_inner"))
                    inner.Add("\t\t"+resource.Value["_inner"]);

                foreach (KeyValuePair<String, String> attribute in resource.Value)
                {
                    if (attribute.Key.StartsWith("_") || attribute.Key == "x:Key") continue;

                    String value = _resolveSyntax(attribute.Value, document);
                    if (!attribute.Key.StartsWith("!") && !value.Contains('\n'))
                        //Simple
                        attributes.Add($"{attribute.Key}=\"{value}\"");
                    else
                    {
                        inner.Add($"\t\t\t<{type}.{attribute.Key}>");
                        inner.Add($"\t\t\t\t{value.TrimEnd('\n').Replace("\n", "\n\t\t\t\t")}");
                        inner.Add($"\t\t\t</{type}.{attribute.Key}>");
                    }
                }

                writer.Append($"\t\t<{type} {String.Join(" ", attributes)}");
                if (inner.Count <= 0)
                    writer.AppendLine(" />");
                if (inner.Count == 1 && !inner[0].Trim().StartsWith("<"))
                    writer.AppendLine($">{inner[0].Trim()}</{type}>");
                else
                    writer.AppendLine($">\n{String.Join("\n", inner)}\n\t\t</{type}>");
            }
            writer.AppendLine( "\t</Styles.Resources>");
            writer.AppendLine();
            foreach ((String selector, Object value) in document.Styles)
				if (value is Dictionary<Object, Object> style)
					WriteStyle(selector, style);
            writer.AppendLine( "</Styles>");


            writer = createFileInCurrentDirectroy($"{pathRoot}.xaml.cs");
            writer.AppendLine( "using Avalonia.Markup.Xaml;");
            writer.AppendLine( "using Avalonia.Styling;");
            writer.AppendLine();
            writer.AppendLine($"namespace {document.Namespace}");
            writer.AppendLine( "{");
            writer.AppendLine($"    public class {document.Name} : Styles");
            writer.AppendLine( "    {");
            writer.AppendLine($"        public {document.Name}() => AvaloniaXamlLoader.Load(this);");
            writer.AppendLine( "    }");
            writer.AppendLine( "}");

			void WriteStyle(String selector, Dictionary<Object, Object> style, Int32 depth = 0)
			{
				if (depth > 0 && !selector.StartsWith('^'))
					selector = $"^{selector}";

				String prepend = new String('\t', depth);
				writer.AppendLine($"{prepend}\t<Style Selector=\"{selector}\">");
				foreach ((Object keyObj, Object value) in style)
					switch ((keyObj, value))
					{
						case (String key, Dictionary<Object, Object> childStyle):
							WriteStyle(key, childStyle, depth + 1);
							break;

						case (String key, Object objValue) when objValue.ToString() is String strValue:
							String parsedValue = _resolveSyntax(strValue, document);
							Boolean isSetter = !key.EndsWith("!");
							if (isSetter && !parsedValue.Contains('\n'))
								//Simple
								writer.AppendLine($"{prepend}\t\t<Setter Property=\"{key}\" Value=\"{parsedValue}\"/>");
							else
							{
								//Complex
								if (isSetter)
									writer.AppendLine($"{prepend}\t\t<Setter Property=\"{key}\">");
								else
									writer.AppendLine($"{prepend}\t\t<{key.Substring(0,key.Length-1)}>");

								writer.AppendLine($"{prepend}\t\t\t{parsedValue.TrimEnd('\n').Replace("\n", $"\n{prepend}\t\t\t")}");

								if (isSetter)
									writer.AppendLine($"{prepend}\t\t</Setter>");
								else
									writer.AppendLine($"{prepend}\t\t</{key.Substring(0,key.Length-1)}>");
							}
							break;
					}

				writer.AppendLine($"{prepend}\t</Style>");
			}
        }
    

        private Regex _syntaxRegex = new Regex(
      @"(
          (?<control>RGBA|HSLA|HSBA)\((?<arg>[^,]+),(?<arg>[^,]+),(?<arg>[^\)]+),(?<arg>[^\)]+)\)
          |
          (?<control>RGB|HSL|HSB)\((?<arg>[^,]+),(?<arg>[^,]+),(?<arg>[^\)]+)\)
          |
          (?<control>\$|@)(?<arg>[\w_\d]*)
        )", RegexOptions.Compiled | RegexOptions.IgnorePatternWhitespace);
        private String _resolveSyntax(String raw, StyleGeneratorDocument document) => _syntaxRegex
            .Replace(raw, m => m.Groups["control"].Value switch
            {
                "$" => document.Constants.ContainsKey(m.Groups["arg"].Value)
                    ?  _resolveSyntax(document.Constants[m.Groups["arg"].Value], document)
                    :  m.Groups[0].Value,

                "@" => $"{{DynamicResource {m.Groups["arg"].Value}}}",

                "RGB" => m.Groups["arg"].Captures.Count == 3
                    ? _convertRgbToHex(m.Groups["arg"].Captures.Select(a => Double.Parse(_resolveSyntax(a.Value, document))).ToList())
                    : m.Groups[0].Value,
                "RGBA" => m.Groups["arg"].Captures.Count == 4
                    ? _convertRgbaToHex(m.Groups["arg"].Captures.Select(a => Double.Parse(_resolveSyntax(a.Value, document))).ToList())
                    : m.Groups[0].Value,

                "HSL" => m.Groups["arg"].Captures.Count == 3
                    ? _convertHslToHex(m.Groups["arg"].Captures.Select(a => Double.Parse(_resolveSyntax(a.Value, document))).ToList())
                    : m.Groups[0].Value,
                "HSLA" => m.Groups["arg"].Captures.Count == 4
                    ? _convertHslaToHex(m.Groups["arg"].Captures.Select(a => Double.Parse(_resolveSyntax(a.Value, document))).ToList())
                    : m.Groups[0].Value,

                "HSB" => m.Groups["arg"].Captures.Count == 3
                    ? _convertHsbToHex(m.Groups["arg"].Captures.Select(a => Double.Parse(_resolveSyntax(a.Value, document))).ToList())
                    : m.Groups[0].Value,
                "HSBA" => m.Groups["arg"].Captures.Count == 4
                    ? _convertHsbaToHex(m.Groups["arg"].Captures.Select(a => Double.Parse(_resolveSyntax(a.Value, document))).ToList())
                    : m.Groups[0].Value,

                _ => m.Groups["control"].Value
            });

        private String _convertRgbToHex(List<Double> args) => SimpleColorTransforms.RgbaToArgb(args[0], args[1], args[2]);
        private String _convertRgbaToHex(List<Double> args) => SimpleColorTransforms.RgbaToArgb(args[0], args[1], args[2], args[3]);
        private String _convertHslToHex(List<Double> args) => SimpleColorTransforms.HslaToArgb(args[0], args[1], args[2]);
        private String _convertHslaToHex(List<Double> args) => SimpleColorTransforms.HslaToArgb(args[0], args[1], args[2], args[3]);
        private String _convertHsbToHex(List<Double> args) => SimpleColorTransforms.HsbaToArgb(args[0], args[1], args[2]);
        private String _convertHsbaToHex(List<Double> args) => SimpleColorTransforms.HsbaToArgb(args[0], args[1], args[2], args[3]);
    }
}
