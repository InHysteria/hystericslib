namespace HystericsLib.Model
{
    public class Container
    {
        private Dictionary<String, Object> _properties = new();

        public T? Get<T>(Definition<T> definition) where T : notnull => Get((Definition)definition) is T value
            ? value
            : default(T);
        public Object? Get(Definition definition)
        {
            String resolvedName = definition.Namespace + "." + definition.Name;
            return _properties.ContainsKey(resolvedName)
                ? _properties[resolvedName]
                : null;
        }

        public Boolean Set<T>(Definition<T> definition, T value) where T : notnull => Set((Definition)definition, (Object)value);
        public Boolean Set(Definition definition, Object value)
        {
            if (Equals(Get(definition), value))
                return false;

            _properties[definition.Namespace + "." + definition.Name] = value;
            return true;
        }
    }
}
