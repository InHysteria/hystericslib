namespace HystericsLib.Model
{
    public record Definition(
        String Name,
        String Namespace = "",
        Boolean IsReadOnly = false
    );
    public record Definition<T> : Definition where T : notnull
    {
        public Definition(String Name, String Namespace = "", Boolean IsReadOnly = false) : base(Name, Namespace, IsReadOnly) { }
    }
}
