using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace HystericsLib.Inputs
{
    public class InputService : IDisposable
    {
        public class KeyEvent
        {
            public Key Key { get; }
            public IReadOnlyCollection<Key> Held { get; }

            public Boolean Control => Held.Contains(Key.LeftControl) || Held.Contains(Key.RightControl);
            public Boolean Shift => Held.Contains(Key.LeftShift) || Held.Contains(Key.RightShift);
            public Boolean Alt => Held.Contains(Key.LeftAlt) || Held.Contains(Key.RightAlt);
            public Boolean Meta => Held.Contains(Key.LeftMeta) || Held.Contains(Key.RightMeta);
            public Boolean Compose => Held.Contains(Key.Compose);

            public KeyEvent(
                Key key,
                IReadOnlyCollection<Key> held)
            {
                Key = key;
                Held = held;
            }
        }

        private List<IListener> _listeners;

        private CancellationTokenSource _cts = new();
        private Thread? _listeningThread = null;

        public event Action<KeyEvent> KeyPressed = delegate { };
        public event Action<KeyEvent> KeyReleased = delegate { };

        public InputService(IEnumerable<IConfig> config)
        {
            _listeners = config
                .Select(x => x.Create())
                .ToList();
        }


        public void BeginListen()
        {
            if (_listeningThread != null) throw new Exception("Service is already listening to input.");

            _listeningThread = new Thread(_listen);
            _listeningThread.Start();
        }
        public void EndListen()
        {
            if (_listeningThread == null) throw new Exception("Service is not listening.");

            _cts.Cancel();
            _listeningThread.Join();
            _listeningThread = null;
        }

        public void Dispose()
        {
            _cts.Cancel();
        }



        private void _listen()
        {
            foreach (IListener listener in _listeners)
            {
                listener.KeyPressed += _listenerInvokedKeyPressed;
                listener.KeyReleased += _listenerInvokedKeyReleased;
            }
            try
            {
                Parallel.ForEach(_listeners, l => l.Listen(_cts.Token));
            }
            finally
            {
                foreach (IListener listener in _listeners)
                {
                    listener.KeyPressed -= _listenerInvokedKeyPressed;
                    listener.KeyReleased -= _listenerInvokedKeyReleased;
                }
            }
        }


        private HashSet<Key> _held { get; } = new();
        private void _listenerInvokedKeyPressed(Key key)
        {
            KeyPressed(new KeyEvent(key, (IReadOnlyCollection<Key>)_held));
            _held.Add(key);
        }
        private void _listenerInvokedKeyReleased(Key key)
        {
            _held.Remove(key);
            KeyReleased(new KeyEvent(key, (IReadOnlyCollection<Key>)_held));
        }
    }
}
