using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace HystericsLib.Inputs.Linux
{
    public class KeyEventListener : InputEventListener, IListener
    {
        public event Action<Key> KeyPressed = delegate { };
        public event Action<Key> KeyReleased = delegate { };

        public KeyEventListener(String inputPath) : base(inputPath) { }

        protected override void OnEventRaised(InputEvent inputEvent)
        {
            if (inputEvent.type != 1) //EV_KEY
                return;

            Key key = (Key)inputEvent.code;
            if (inputEvent.value == 1)
                KeyPressed(key);
            else
                KeyReleased(key);
        }
    }
}
