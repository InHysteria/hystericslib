using System;

namespace HystericsLib.Inputs.Linux
{
    public class EventConfig : IConfig
    {
        public String Name { get; set; } = "";
        public String Path { get; set; } = "";

        public IListener Create() => new KeyEventListener(Path);
    }
}
