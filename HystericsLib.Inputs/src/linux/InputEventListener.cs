using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace HystericsLib.Inputs.Linux
{
    public class InputEventListener : IDisposable
    {
        public event Action<InputEvent> EventRaised = delegate { };

        private String _inputPath;
        private Boolean _stop = false;
        private FileStream? _fs;

        public InputEventListener(String inputPath) => _inputPath = inputPath;

        public void Listen(CancellationToken cancelToken)
        {
            Byte[] buffer = new Byte[_inputEventSize];

            _fs = new FileStream(_inputPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            _stop = false;

            while (!_stop && !cancelToken.IsCancellationRequested)
            {
                Task<Int32> readTask = _fs.ReadAsync(buffer, 0, _inputEventSize, cancelToken);
                while (!readTask.IsCompleted && !_stop && !cancelToken.IsCancellationRequested)
                    Thread.Sleep(0); //Spin wait

                if (cancelToken.IsCancellationRequested || _stop)
                    return;

                InputEvent evt = _parseEvent(buffer);
                EventRaised(evt);
                OnEventRaised(evt);
            }
        }

        protected virtual void OnEventRaised(InputEvent inputEvent) { }

        public void Dispose()
        {
            _stop = true;
            _fs?.Dispose();
            _fs = null;
        }


        private static readonly Int32 _inputEventSize = Marshal.SizeOf(typeof(InputEvent));
        public struct InputEvent
        {
            public UInt64 __sec;
            public UInt64 __usec;

            public UInt16 type;
            public UInt16 code;
            public UInt32 value;
        }
        private static InputEvent _parseEvent(Byte[] data)
        {
            GCHandle handle = GCHandle.Alloc(data, GCHandleType.Pinned);
            try
            {
                Object? box = Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(InputEvent));
                if (box is InputEvent evt)
                    return evt;

                throw new Exception("Data was not the expected type.");
            }
            finally
            {
                handle.Free();
            }
        }
    }
}
