using System;

namespace HystericsLib.Inputs
{
    public interface IConfig
    {
        IListener Create();
    }
}
