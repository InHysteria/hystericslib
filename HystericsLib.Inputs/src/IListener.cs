using System;
using System.Threading;

namespace HystericsLib.Inputs
{
    public interface IListener
    {
        event Action<Key> KeyPressed;
        event Action<Key> KeyReleased;

        void Listen(CancellationToken cancelToken);
    }
}
