using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace HystericsLib
{
    public static class Debug
    {
        [Conditional("DEBUG")]
        public static void Print(
            String message = "",
            [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0) =>
            Console.WriteLine($"[{DateTime.Now.ToString("T")}] {sourceFilePath}:{sourceLineNumber}:{memberName}() {message}");

        [Conditional("DEBUG")]
        public static void Print(
            Exception e,
            [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0)
        {
            Exception? head = e;
            do
            {
                ConsoleColor restore = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"[{DateTime.Now.ToString("T")}] {sourceFilePath}:{sourceLineNumber}:{memberName}() {(head?.GetType().ToString() ?? "<NULL>")} - {(head?.Message ?? "<NULL>")}");
                if (!String.IsNullOrEmpty(head?.StackTrace))
                    Console.WriteLine($"\t\t{(head?.StackTrace ?? "<NULL>").Replace("\n","\n\t\t")}");
                Console.ForegroundColor = restore;
            } while ((head = head?.InnerException) != null);
        }
    }
}
