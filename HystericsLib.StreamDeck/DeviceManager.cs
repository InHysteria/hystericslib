using HystericsLib.StreamDeck.Devices;

namespace HystericsLib.StreamDeck
{
    public interface IDeviceManager
    {
        IEnumerable<IStreamDeck> Find();
        IEnumerable<T> Find<T>() where T : Devices.StreamDeck;
    }
    public class DeviceManager : IDeviceManager, IDisposable
    {
        private const Int32 VENDOR_ID = 0x0fd9; //4057
        private UsbContext? _context { get; set; } = new();

        public IEnumerable<IStreamDeck> Find() => new IEnumerable<Devices.StreamDeck>[]
        {
            Find<StreamDeckOriginal>(),
            //Find<StreamDeckOriginalV2>(),
            Find<StreamDeckMini>(),
            Find<StreamDeckXl>(),
            Find<StreamDeckMk2>(),
            Find<StreamDeckPedal>()
        }   .SelectMany(x => x);

        public IEnumerable<T> Find<T>() where T : Devices.StreamDeck
        {
            if (_context == null) yield break;

            ProductCode productCode = _map(typeof(T));
            if (productCode == ProductCode.Empty) yield break;

            //Ideally we'd do this, but UsbDeviceFinder has some internal issues todo with checking when a value hasn't been set.
            //UsbDeviceFinder spec = new UsbDeviceFinder(VENDOR_ID, (Int32)productCode);
            //using UsbDeviceCollection collection = context.FindAll(spec);

            using UsbDeviceCollection collection = _context.List();
            foreach (IUsbDevice device in collection.Where(d => d.Info.VendorId == VENDOR_ID && d.Info.ProductId == (Int32)productCode))
                if (_map(productCode, device) is T streamdeckDevice)
                    yield return streamdeckDevice;
        }

        internal IUsbDevice? _reattach(IStreamDeck deck)
        {
            if (_context == null) return null;
            if (deck.SerialNumber == null) return null; //Device must be opened at least once to get a serial number.

            using UsbDeviceCollection collection = _context.List();
            foreach (IUsbDevice device in collection)
            {
                if (!device.TryOpen()) continue;
                try
                {
                    if (device.Info.SerialNumber == deck.SerialNumber)
                        return device.Clone();
                }
                finally
                {
                    device.Close();
                }
            }
            return null;
        }

        private static ProductCode _map(Type type) => type switch
        {
            _ when type == typeof(StreamDeckOriginal) => ProductCode.Original,
            //_ where type == typeof(StreamDeckOriginalV2) => ProductCode.OriginalV2,
            _ when type == typeof(StreamDeckMini) => ProductCode.Mini,
            _ when type == typeof(StreamDeckXl) => ProductCode.Xl,
            _ when type == typeof(StreamDeckMk2) => ProductCode.Mk2,
            _ when type == typeof(StreamDeckPedal) => ProductCode.Pedal,

            _ => ProductCode.Empty
        };
        private Object? _map(ProductCode productCode, IUsbDevice device) => productCode switch
        {
            ProductCode.Original => new StreamDeckOriginal(this, device.Clone()),
            ProductCode.OriginalV2 => new StreamDeckMk2(this, device),
            ProductCode.Mini => new StreamDeckMini(this, device.Clone()),
            ProductCode.Xl => new StreamDeckXl(this, device.Clone()),
            ProductCode.Mk2 => new StreamDeckMk2(this, device.Clone()),
            ProductCode.Pedal => new StreamDeckPedal(this, device.Clone()),

            _ => null
        };

        public void Dispose()
        {
            if (_context == null) return;

            _context.Dispose();
            _context = null;
        }
    }
}
