namespace HystericsLib.StreamDeck
{
    public class StreamDeckException : Exception
    {
        public Devices.IStreamDeck SourceDeck { get; }

        public StreamDeckException(String message, Devices.IStreamDeck source) : base(message) => SourceDeck = source;
        public StreamDeckException(String message, Exception innerException, Devices.IStreamDeck source) : base(message, innerException) => SourceDeck = source;
    }
}
