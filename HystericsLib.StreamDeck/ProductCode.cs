namespace HystericsLib.StreamDeck
{
    public enum ProductCode
    {
        Empty = 0,
        Original = 0x0060,
        OriginalV2 = 0x006d,
        Mini = 0x0063,
        Xl = 0x006c,
        Mk2 = 0x0080,
        Pedal = 0x0086
    }
}
