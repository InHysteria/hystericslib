namespace HystericsLib.StreamDeck.Devices
{
    public record Size(Int32 Width, Int32 Height);
    public record ImageFormat(Size Resolution, ImageType ImageType, Int32 Quality, Boolean FlipHorizontal, Boolean FlipVertical);
    public record Specification(Size DisplaySize, ImageFormat? ImageFormat, ProductCode ProductCode);

    public enum ImageType
    {
        Bmp, //Not Supported, blamek SkiaSharp

        Jpeg,
        Png,
        Webp
    }
}
