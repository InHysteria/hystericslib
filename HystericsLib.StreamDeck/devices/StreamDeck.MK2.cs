namespace HystericsLib.StreamDeck.Devices
{
    public class StreamDeckMk2 : StreamDeck
    {
        public override Specification Specification { get; } = new Specification(
            new Size(5, 3),
            new ImageFormat(
                new Size(72, 72),
                ImageType.Jpeg, 90,
                true, true),

            ProductCode.Mk2
        );

        protected override Int32 keySpaceOffset { get; } = 4;
        protected override Int32 bufferSize { get; } = 1024;

        protected override void send_SetKeyImage(Byte key, Byte[] data)
        {
            Int32 page = 0;
            Int32 cursor = 0;

            while (cursor < data.Length)
            {
                Byte[] payload = new Byte[bufferSize - 8];
                Int32 nextPayloadSize = Math.Min(payload.Length, data.Length - cursor);
                Array.Copy(data, cursor, payload, 0, nextPayloadSize);

                write(new Byte[] {
                    0x02, 0x07,
                    key,
                    (Byte)(cursor + payload.Length > data.Length
                        ? 1
                        : 0),
                    (Byte)(nextPayloadSize & 0xFF),
                    (Byte)(nextPayloadSize >> 8),
                    (Byte)(page & 0xFF),
                    (Byte)(page >> 8)
                    },
                    payload);

                cursor += payload.Length;
                page++;
            }
        }

        public StreamDeckMk2(DeviceManager manager, IUsbDevice device) : base(manager, device) {}
    }
}
