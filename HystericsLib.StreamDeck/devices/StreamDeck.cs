namespace HystericsLib.StreamDeck.Devices
{
    public delegate void StreamDeckEvent(IStreamDeck source);
    public delegate void StreamDeckEvent<T>(IStreamDeck source, T args);

    public class ImageOptions
    {
        public Boolean ScaleDown = true;
        public Boolean ScaleUp = false;
        public Boolean Center = true;
        public ImageScaleApproach ScaleApproach = ImageScaleApproach.Fill;
        public Int32 Margin = 5;
    }

    public enum ImageScaleApproach
    {
        Fill,
        Uniform,
        UniformToFill
    }

    public interface IStreamDeck : IDisposable
    {
        Specification Specification { get; }
        ProductCode ProductCode { get; }
        String? SerialNumber { get; }

        Boolean IsOpen { get; }
        Boolean IsAttached { get; }
        Double PollFrequency { get; }

        event StreamDeckEvent<(Int32 X, Int32 Y)> KeyPressed;
        event StreamDeckEvent Attached;
        event StreamDeckEvent Detached;


        void SetImage((Int32 x, Int32 y) key, Stream image, ImageOptions options);
        void SetImage((Int32 x, Int32 y) key, Byte[] image);
        void SetBrightness(Double percent);
        void SetBrightness(Byte percent);

        void Open();
        void Close();
    }
    public abstract class StreamDeck : IStreamDeck
    {
        private DeviceManager _manager { get; }
        private IUsbDevice? _device { get; set; }
        private Timer? _reattachTimer { get; set; }
        private Timer? _timer { get; set; }
        private Int32 _openInterface { get; set; }

        protected abstract Int32 keySpaceOffset { get; }
        protected abstract Int32 bufferSize { get; }
        protected virtual Int32 noOfKeys => Specification.DisplaySize.Width * Specification.DisplaySize.Height;

        public abstract Specification Specification { get; }
        public String? SerialNumber { get; set; }
        public ProductCode ProductCode { get; }

        public Double PollFrequency { get; protected set; } = 20.0;

        private Boolean _isAttached = true;
        public Boolean IsAttached
        {
            get => _isAttached;
            private set
            {
                if (_isAttached == value) return;

                _isAttached = value;
                if (value)
                    Attached(this);
                else
                {
                    _device = null;
                    _reader = null;
                    _writer = null;
                    _timer?.Dispose();
                    _timer = null;
                    Detached(this);
                }
            }
        }
        public Boolean IsOpen => _device != null && _timer != null;

        public StreamDeck(DeviceManager manager, IUsbDevice device)
        {
            _manager = manager;
            _device = device;

            ProductCode = (ProductCode)device.ProductId;

            _reattachTimer = new Timer(_reattach, null, 0, 1000);
        }

        public event StreamDeckEvent<(Int32 X, Int32 Y)> KeyPressed = delegate { };
        public event StreamDeckEvent Attached = delegate { };
        public event StreamDeckEvent Detached = delegate { };

        private void _timerElapsed(Object? state)
        {
            try
            {
                if (!IsAttached)
                    _reattach();

                if (!IsOpen) return;

                Int32 i = 0;
                foreach (Boolean pressed in read_Keys())
                {
                    if (pressed)
                        KeyPressed(this, (i % Specification.DisplaySize.Width, i / Specification.DisplaySize.Width));

                    i++;
                }

            }
            catch (Exception e)
            {
                List<String> errors = new();
                Exception? head = e;

                for (; head != null; head = head?.InnerException)
                    errors.Add($"\t{head?.GetType()}: {head?.Message}\n\t{head?.StackTrace?.Replace("\n", "\n\t\t")}");

                Console.WriteLine("An error occured on the read thread..");
                Console.WriteLine(String.Join("\n->\n", errors));
                throw;
            }
        }

        public void SetImage((Int32 x, Int32 y) key, Stream sourceData, ImageOptions options)
        {
            if (Specification.ImageFormat == null) return;

            using SKBitmap source = SKBitmap.Decode(sourceData);
            using SKBitmap image = new SKBitmap(new SKImageInfo(Specification.ImageFormat.Resolution.Width, Specification.ImageFormat.Resolution.Height));
            using SKCanvas canvas = new SKCanvas(image);

            canvas.Scale(
                (Specification.ImageFormat.FlipHorizontal ? -1f : 1f),
                (Specification.ImageFormat.FlipVertical ? -1f : 1f),
                image.Width / 2f,
                image.Height / 2f
            );

            Single scaleX = Math.Max(options.ScaleDown ? Single.MinValue : 1f, Math.Min(options.ScaleUp ? Single.MaxValue : 1f, (Single)(image.Width - options.Margin * 2) / (Single)source.Width));
            Single scaleY = Math.Max(options.ScaleDown ? Single.MinValue : 1f, Math.Min(options.ScaleUp ? Single.MaxValue : 1f, (Single)(image.Height - options.Margin * 2) / (Single)source.Height));

            switch (options.ScaleApproach)
            {
                case ImageScaleApproach.Fill: break;
                case ImageScaleApproach.Uniform:
                    scaleX = Math.Min(scaleX, scaleY);
                    scaleY = scaleX;
                    break;
                case ImageScaleApproach.UniformToFill:
                    scaleX = Math.Max(scaleX, scaleY);
                    scaleY = scaleX;
                    break;
            }

            canvas.Scale(scaleX, scaleY);
            if (options.Center)
                canvas.Translate(
                    ((image.Width - (source.Width * scaleX)) / scaleX) * 0.5f,
                    ((image.Height - (source.Height * scaleY)) / scaleY) * 0.5f);

            canvas.DrawBitmap(source, new SKPoint(0,0));

            using MemoryStream data = new();
            using SKDynamicMemoryWStream skOut = new();
            image.Encode(
                skOut,
                Specification.ImageFormat.ImageType switch
                {
                    ImageType.Bmp => SKEncodedImageFormat.Bmp, //Not actually supported by SkiaSharp, needs looking into
                    ImageType.Jpeg => SKEncodedImageFormat.Jpeg,
                    ImageType.Png => SKEncodedImageFormat.Png,
                    ImageType.Webp => SKEncodedImageFormat.Webp,

                    _ => throw new NotSupportedException()
                },
                Specification.ImageFormat.Quality);

            skOut.Flush();
            skOut.CopyTo(data);

            SetImage(key, data.ToArray());
        }
        public void SetImage((Int32 x, Int32 y) key, Byte[] image) => send_SetKeyImage((Byte)((key.y * Specification.DisplaySize.Width) + key.x), image);
        public void SetBrightness(Double percent) => send_SetBrightness((Byte)(percent * 100));
        public void SetBrightness(Byte percent) => send_SetBrightness(percent);

        protected virtual void send_Reset() => write(new Byte[] { 0x03, 0x02 });
        protected virtual void send_ResetKeyStream() => write(new Byte[] { 0x02 });
        protected virtual void send_SetBrightness(Byte percent) => write_feature(new Byte[] { 0x03, 0x08, Math.Max((Byte)0, Math.Min((Byte)100, percent)) });
        protected virtual void send_SetKeyImage(Byte key, Byte[] data) => throw new NotSupportedException();

        protected virtual IEnumerable<Boolean> read_Keys()
        {
            read(noOfKeys + keySpaceOffset, out Byte[] data);
            for (Int32 i = 0; i < noOfKeys; i++)
                yield return data[keySpaceOffset + i] != 0;
        }

        public void Open()
        {
            if (_device == null) return;
            if (IsOpen) return;

            try
            {
                _device.Open();
                _device.ClaimInterface(0);
                send_ResetKeyStream();

                _timer = new Timer(_timerElapsed, null, 0, (Int32)(1000.0 / PollFrequency));
                SerialNumber = _device.Info.SerialNumber;

            }
            catch (Exception e)
            {
                throw new StreamDeckException("An unhandled exception occured while opening the device.", e, this);
            }
        }

        public void Close()
        {
            if (_device == null) return;
            if (!IsOpen) return;
            if (!_device.IsOpen) throw new StreamDeckException("Cannot close an unopened device.", this);

            try
            {
                _reader = null;
                _writer = null;
                _device.ReleaseInterface(0);
                _device.Close();
                _timer?.Dispose();
                _timer = null;
            }
            catch (Exception e)
            {
                throw new StreamDeckException("An unhandled exception occured while opening the device.", e, this);
            }
        }

        private void _reattach(Object? _ = null)
        {
            if (_device != null) return;

            _device = _manager._reattach(this);
            if (_device != null) //Device was reattached
                IsAttached = true;
        }

        private UsbEndpointReader? _reader { get; set; }
        protected Int32 read(Int32 length, out Byte[] data) => read(data = new Byte[length]);
        protected Int32 read(Byte[] data) => read(data, 0, data.Length);
        protected Int32 read(Byte[] data, Int32 offset, Int32 count)
        {
            if (_device == null || !IsOpen) return 0;
            if (count + offset > data.Length) throw new StreamDeckException("Provided byte array is to small to recieve the requested quantity of data.", this);
            if (count > bufferSize) throw new StreamDeckException($"This device only supports reading up to {bufferSize} bytes per operation.", this);

            try
            {
                Byte[] buffer = new Byte[bufferSize];
                _reader ??= _device.OpenEndpointReader(ReadEndpointID.Ep01);
                var error = _reader.Read(buffer, 0, bufferSize, (Int32)(1000.0 / PollFrequency), out Int32 bytesRead);
                if (error == Error.NoDevice)
                    IsAttached = false;

                Array.Copy(buffer, 0, data, offset, count);

                return Math.Min(count, bytesRead);
            }
            catch (Exception e)
            {
                throw new StreamDeckException("An exception occured while attempting to read from the device.", e, this);
            }
        }

        private const Int32 _WRITE_TIMEOUT = 5000;
        private UsbEndpointWriter? _writer { get; set; }
        protected Int32 write(Byte[] payload) => write(payload, 0, payload.Length);
        protected Int32 write(params Byte[][] blocks)
        {
            Byte[] payload = new Byte[blocks.Sum(b => b.Length)];
            Int32 offset = 0;
            foreach (Byte[] block in blocks)
            {
                block.CopyTo(payload, offset);
                offset += block.Length;
            }

            return write(payload);
        }
        protected Int32 write(Byte[] payload, Int32 offset, Int32 count)
        {
            if (_device == null || !IsOpen) return 0;
            if (count > bufferSize) throw new StreamDeckException($"This device only supports writting up to {bufferSize} bytes per operation.", this);

            try
            {
                Byte[] buffer = new Byte[bufferSize];
                Array.Copy(payload, offset, buffer, 0, count);

                _writer ??= _device.OpenEndpointWriter(WriteEndpointID.Ep02);
                Error error = _writer.Write(buffer, 0, bufferSize, _WRITE_TIMEOUT, out Int32 bytesWritten);
                if (error == Error.NoDevice)
                    IsAttached = false;

                return bytesWritten;
            }
            catch (Exception e)
            {
                throw new StreamDeckException("An exception occured while attempting to write to the device.", e, this);
            }
        }

        protected void write_feature(Byte[] payload)
        {
            if (_device == null || !IsOpen) return;
            if (payload.Length > bufferSize) throw new StreamDeckException($"The provided payload is too long for a setup packet.", this);

            UsbSetupPacket header = new UsbSetupPacket(0x21, 0x09, 0x0303, 0, 33);
            Byte[] data = new Byte[32];
            Array.Copy(payload, 0, data, 0, payload.Length);

            Error error = (Error)_device.ControlTransfer(header, data, 0, data.Length);
            if (error == Error.NoDevice)
                IsAttached = false;
        }

        public void Dispose()
        {
            try
            {
                Close();
            }
            catch {}
            finally
            {
                _reattachTimer?.Dispose();
                _reattachTimer = null;
                _device = null;
            }
        }
    }
}
