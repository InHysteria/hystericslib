namespace HystericsLib.StreamDeck.Devices
{
    public class StreamDeckMini : StreamDeck
    {
        public override Specification Specification { get; } = new Specification(
            new Size(3, 2),
            new ImageFormat(
                new Size(80, 80),
                ImageType.Bmp, 90,
                false, true),

            ProductCode.Mini
        );

        protected override Int32 keySpaceOffset { get; } = 1;
        protected override Int32 bufferSize { get; } = 1024;

        public StreamDeckMini(DeviceManager manager, IUsbDevice device) : base(manager, device) {}
    }
}
