namespace HystericsLib.StreamDeck.Devices
{
    public class StreamDeckPedal : StreamDeck
    {
        public override Specification Specification { get; } = new Specification(
            new Size(3, 1),
            null,

            ProductCode.Pedal
        );

        protected override Int32 keySpaceOffset { get; } = 4;
        protected override Int32 bufferSize { get; } = 1024;

        public StreamDeckPedal(DeviceManager manager, IUsbDevice device) : base(manager, device) {}
    }
}
