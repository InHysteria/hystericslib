namespace HystericsLib.StreamDeck.Devices
{
    public class StreamDeckOriginal : StreamDeck
    {
        public override Specification Specification { get; } = new Specification(
            new Size(5, 3),
            new ImageFormat(
                new Size(72, 72),
                ImageType.Bmp, 90,
                true, true),

            ProductCode.Original
        );

        protected override Int32 keySpaceOffset { get; } = 1;
        protected override Int32 bufferSize { get; } = 8191;

        public StreamDeckOriginal(DeviceManager manager, IUsbDevice device) : base(manager, device) {}
    }
}
