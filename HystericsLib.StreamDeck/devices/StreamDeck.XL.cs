namespace HystericsLib.StreamDeck.Devices
{
    public class StreamDeckXl : StreamDeck
    {
        public override Specification Specification { get; } = new Specification(
            new Size(5, 3),
            new ImageFormat(
                new Size(96, 96),
                ImageType.Jpeg, 90,
                true, true),

            ProductCode.Xl
        );

        protected override Int32 keySpaceOffset { get; } = 4;
        protected override Int32 bufferSize { get; } = 1024;

        public StreamDeckXl(DeviceManager manager, IUsbDevice device) : base(manager, device) {}
    }
}
