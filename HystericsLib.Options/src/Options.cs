using System;
using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;

namespace HystericsLib.Options
{
    public class Options
    {
        private Int32 _extraPosition = 0;
        private ArgumentSet _args { get; }

        protected Options(String[] args) : this(new ArgumentSet(args)) { }
        protected Options(String args) : this(new ArgumentSet(args)) { }
        protected Options(ArgumentSet args) => _args = args;

        public static Options For(params String[] args) => new Options(args);
        public static Options For(String args) => new Options(args);

        public ParseOptions Parse(String key) => new ParseOptions(
            this,
            () => _args.HasKey(key),
            () => _args.GetValue(key));
        public ParseOptions Parse(params String[] keys) => new ParseOptions(this,
            () => keys.Any(key => _args.HasKey(key)),
            () => _args.GetValue(keys.First(key => _args.HasKey(key))));

        public ParseOptions ParseExtra() => new ParseOptions(this,
            () => _extraPosition < _args.Extra.Count(),
            () => _args.Extra.Skip(_extraPosition++).First());


        public List<String> GetRemainingExtras() => new List<String>(_args.Extra.Skip(_extraPosition));

        public class ParseOptions
        {
            private Options _parent { get; }
            private Func<Boolean> _predicate { get; }
            private Func<String?> _value { get; }

            public ParseOptions(Options parent, Func<Boolean> predicate, Func<String?> value)
            {
                _parent = parent;
                _predicate = predicate;
                _value = value;
            }

            public Options Into(out String? field)
            {
                field = default;
                if (_predicate())
                    field = _value();

                return _parent;
            }
            public Options Into(out Boolean field)
            {
                field = false;
                if (_predicate())
                    field = true;

                return _parent;
            }
            public Options With(Action<String?> func)
            {
                if (_predicate())
                    func(_value());

                return _parent;
            }
        }
    }
}
