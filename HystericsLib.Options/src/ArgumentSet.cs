using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace HystericsLib.Options
{
    public class ArgumentSet
    {
        private Dictionary<String, String?> _args { get; }
        private List<String> _extra { get; }

        private const String _KEY_GROUP = "key";
        private const String _VAL_GROUP = "value";
        private const String _EXT_GROUP = "extra";
        private static readonly Regex _argRegex = new Regex($@"
            (
                (
                    -(?<{_KEY_GROUP}>[^\s-=])
                    |
                    --(?<{_KEY_GROUP}>[^\s=]+)
                )
                (
                    [\s=](?<{_VAL_GROUP}>[^\s-""][^\s""]+)
                    |
                    [\s=]""(?<{_VAL_GROUP}>[^""]+)""
                    |
                    \s*
                )
                |
                (?<{_EXT_GROUP}>[^\s""]+)
                |
                ""(?<{_EXT_GROUP}>[^""]+)""
            )
        ", RegexOptions.Compiled | RegexOptions.IgnorePatternWhitespace);

        public ArgumentSet(params String[] args) : this(String.Join(" ", args.Select(a => a.Contains(" ") ? $"\"{a}\"" : a))) {}
        public ArgumentSet(String args)
        {
            _args = new();
            _extra = new();

            foreach (Match match in _argRegex.Matches(args))
                if (match.Groups[_KEY_GROUP].Success && match.Groups[_VAL_GROUP].Success)
                    //Pair
                    _args[match.Groups[_KEY_GROUP].Value.ToString()] = match.Groups[_VAL_GROUP].Value.ToString();
                else if (match.Groups[_KEY_GROUP].Success) //&& !match.Groups[_VAL_GROUP].Success)
                    //Flag
                    _args[match.Groups[_KEY_GROUP].Value.ToString()] = default(String);
                else if (match.Groups[_EXT_GROUP].Success)
                    //Extra
                    _extra.Add(match.Groups[_EXT_GROUP].Value.ToString());
        }

        public IEnumerable<String> Keys => _args.Keys.AsEnumerable();
        public IEnumerable<String> Extra => _extra.AsEnumerable();

        public Boolean HasKey(String key) => _args.ContainsKey(key);
        public Boolean HasKey(Func<String, Boolean> condition) => _args.Keys.Any(condition);
        public Boolean HasValue(String key) => HasKey(key) && _args[key] != default(String);

        public String? GetValue(String key) => HasKey(key) ? _args[key] : default(String);
    }
}
