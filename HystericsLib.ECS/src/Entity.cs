namespace HystericsLib.ECS;

public class Entity : IEquatable<Entity>
{
	public Guid ID { get; private set; }

	private EntityContext _parent { get; }

	internal Entity(Guid id, EntityContext parent) => (ID, _parent) = (id, parent);

	public Boolean Has<T>() where T : Component => _parent.EntityHasComponent<T>(this);
	public T Get<T>() where T : Component => _parent.GetEntityComponent<T>(this);
	public IEnumerable<T> GetAll<T>() where T : Component => _parent.GetEntityComponents<T>(this);
	public T Add<T>(T component) where T : Component
	{
		_parent.Attach(this, component);
		return component;
	}
	public T Remove<T>(T component) where T : Component
	{
		_parent.Detatch(this, component);
		return component;
	}

	public ReadOnlyCollection<Component> Components => _parent.ComponentsOf(this);

	public Boolean Equals(Entity? other) => ID.Equals(other?.ID);
	public override Int32 GetHashCode() => ID.GetHashCode();
	public override Boolean Equals(Object? obj) => obj is Entity other && ID.Equals(other.ID);

	public static Boolean operator !=(Entity? a, Entity? b) => a?.ID != b?.ID;
	public static Boolean operator ==(Entity? a, Entity? b) => a?.ID == b?.ID;
}