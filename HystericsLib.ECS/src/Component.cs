namespace HystericsLib.ECS;

public abstract class Component : IEquatable<Component>
{
	private Type _ID => GetType();

	public Boolean Equals(Component? other) => _ID.Equals(other?._ID);
	public override Int32 GetHashCode() => _ID.GetHashCode();
	public override Boolean Equals(Object? obj) => obj is Component other && _ID.Equals(other._ID);

	public static Boolean operator !=(Component? a, Component? b) => a?._ID != b?._ID;
	public static Boolean operator ==(Component? a, Component? b) => a?._ID == b?._ID;
}