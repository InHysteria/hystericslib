using System;
using System.Collections.ObjectModel;

namespace HystericsLib.ECS;

public class EntityContext
{
	private List<Service> _services { get; } = new();
	private Dictionary<Entity, HashSet<Component>> _entityToComponentLookup { get; } = new();
	private Dictionary<Component, HashSet<Entity>> _componentToEntityLookup { get; } = new();

	public Entity New(params Component[] components) => New(Guid.NewGuid(), components);
	public Entity New(Guid id, params Component[] components)
	{
		Entity entity = new(id, this);

		_entityToComponentLookup[entity] = new();

		Attach(entity, components);

		return entity;
	}

	public void Attach(Entity entity, params Component[] components)
	{
		if (!_entityToComponentLookup.ContainsKey(entity))
			throw new NullReferenceException($"Entity:{entity.ID} is not tracked by this context.");

		foreach (Component component in components)
		{
			_entityToComponentLookup[entity].Add(component);
			if (!_componentToEntityLookup.ContainsKey(component))
				_componentToEntityLookup[component] = new() { entity };
			else
				_componentToEntityLookup[component].Add(entity);
		}
		
		UpdateInterestedServices(entity);
	}	

	public void Detatch(Entity entity, params Component[] components)
	{
		if (!_entityToComponentLookup.ContainsKey(entity))
			throw new NullReferenceException($"Entity:{entity.ID} is not tracked by this context.");

		foreach (Component component in components)
		{
			_entityToComponentLookup[entity].Remove(component);
			if (_componentToEntityLookup.ContainsKey(component))
				_componentToEntityLookup[component].Remove(entity);
		}

		UpdateInterestedServices(entity);
	}

	public void Delete(Entity entity)
	{
		if (_entityToComponentLookup.ContainsKey(entity))
		{
			foreach (Component component in ComponentsOf(entity))
				if (_componentToEntityLookup.ContainsKey(component))
					_componentToEntityLookup[component].Remove(entity);

			_entityToComponentLookup.Remove(entity);			

			foreach (Service service in _services)
				service.Remove(entity);
		}
	}

	public void AddService(Service service)
	{
		_services.Add(service);

		foreach (Entity entity in _entityToComponentLookup.Keys)
			if (service.InterestedIn(entity))
				service.Track(entity);
			else
				service.Remove(entity);
	}

	public void RemoveService(Service service)
	{
		_services.Remove(service);
	}

	private void UpdateInterestedServices(Entity entity)
	{			
		foreach (Service service in _services)
			if (service.InterestedIn(entity))
				service.Track(entity);
			else
				service.Remove(entity);
	}

	public Boolean EntityHasComponent<T>(Entity entity) where T : Component => _entityToComponentLookup.ContainsKey(entity) && _entityToComponentLookup[entity].Any(c => c is T);
	public IEnumerable<T> GetEntityComponents<T>(Entity entity) where T : Component => _entityToComponentLookup[entity].OfType<T>();
	public T GetEntityComponent<T>(Entity entity) where T : Component => GetEntityComponents<T>(entity).First();

	public Boolean HasEntity(Guid id) => _entityToComponentLookup.ContainsKey(new Entity(id, this));
	public Entity GetEntity(Guid id) => _entityToComponentLookup.First(x => x.Key.ID == id).Key;

	public ReadOnlyCollection<Component> ComponentsOf(Entity entity) => _entityToComponentLookup.ContainsKey(entity)
		? new ReadOnlyCollection<Component>(_entityToComponentLookup[entity].ToArray())
		: new List<Component>().AsReadOnly();

	public IEnumerable<Entity> All() => _entityToComponentLookup.Keys;
	public IEnumerable<Entity> AllWith<T>() where T : Component => _componentToEntityLookup
		.Where(p => p.Key is T)
		.SelectMany(p => p.Value)
		.Distinct();

}