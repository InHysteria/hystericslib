using System.Collections;

namespace HystericsLib.ECS;

public abstract class Service
{
	private HashSet<Entity> _tracked { get; } = new();
	protected IEnumerable<Entity> Tracked => _tracked;

	public abstract Boolean InterestedIn(Entity entity);

	internal void Track(Entity entity) 
	{ 
		lock (Tracked) { _tracked.Add(entity); } 
		OnAddedTracking(entity); 
	}
	internal void Remove(Entity entity) 
	{ 
		lock (Tracked) { _tracked.Remove(entity); }
		OnRemovedTracking(entity); 
	}

	protected virtual void OnAddedTracking(Entity entity) {}
	protected virtual void OnRemovedTracking(Entity entity) {}
}