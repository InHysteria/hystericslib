global using System;
global using System.Linq;
global using System.Threading;
global using System.Threading.Tasks;
global using System.Collections.Generic;
global using System.Collections.ObjectModel;
global using System.ComponentModel;

global using Avalonia;
global using Avalonia.Data;
global using Avalonia.Input;
global using Avalonia.Markup.Xaml;
global using Avalonia.Markup.Xaml.Styling;
global using Avalonia.Styling;
global using Avalonia.Controls;
global using Avalonia.Controls.Templates;
global using Avalonia.Controls.ApplicationLifetimes;
global using Avalonia.Threading;

global using Autofac;