using System;
using System.Windows.Input;
using System.Threading.Tasks;

namespace HystericsLib.Avalonia
{
    public class LambdaAsyncCommand<T> : LambdaAsyncCommand
    {
        public LambdaAsyncCommand(Func<T, Task> execute, Func<T, Boolean>? canExecute = null) : base(
            (parameter) => execute((T)parameter!),
            (parameter) => parameter is T tParameter && (canExecute?.Invoke(tParameter) ?? true)
        ) {}
    }

    public class LambdaAsyncCommand : LambdaCommand
    {
		private Boolean _isExecuting { get; set; }
		private Func<Object?, Task> _executeAsync { get; }

        public LambdaAsyncCommand(Func<Object?, Task> executeAsync, Func<Object?, Boolean>? canExecute = null) : base(
			_ => {},
			canExecute)
		{
			_isExecuting = false;
			_executeAsync = executeAsync;
			_execute = ExecuteAsync;
		}

		private async void ExecuteAsync(Object? o)
		{
			_isExecuting = true;
			RaiseCanExecuteChanged();

			await _executeAsync(o);

			_isExecuting = false;
			RaiseCanExecuteChanged();
		}
    }
}