using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

using HystericsLib;

namespace HystericsLib.Avalonia.Dialogs
{
    [ResolvesTo(typeof(StringPromptDialogView))]
    public class StringPromptDialogViewModel : DialogViewModel<String>
    {
        private Predicate<String> _predicate { get; }
		
		private String _caption = "";
        public String Caption
		{
			get => _caption;
			set => Set(ref _caption, value);
		}

        private String _input = "";
        public String Input
        {
            get => _input;
            set => Set(ref _input, value);
        }

        private String _okText = "OK";
        public String OkText
        {
            get => _okText;
            set => Set(ref _okText, value);
        }

        private String _cancelText = "Cancel";
        public String CancelText
        {
            get => _cancelText;
            set => Set(ref _cancelText, value);
        }

        private Boolean _showOk = true;
        public Boolean ShowOk
        {
            get => _showOk;
            set => Set(ref _showOk, value);
        }

        private Boolean _showCancel = true;
        public Boolean ShowCancel
        {
            get => _showCancel;
            set => Set(ref _showCancel, value);
        }

        public StringPromptDialogViewModel(String title = "Prompt", Predicate<String>? predicate = null)
        {
            Title = title;
            Fallback = "";
            _predicate = predicate ?? (s => true);
        }

        protected override String OnComplete(Object? context) => (context is String s) ? s : "";
        protected override Boolean CanComplete(Object? context) => context is String s && _predicate(s);
    }
}
