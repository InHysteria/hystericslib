using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace HystericsLib.Avalonia.Dialogs
{
    public class SelectorDialogView : UserControl
    {
        public SelectorDialogView()
        {
            this.InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
