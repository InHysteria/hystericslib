using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

using HystericsLib;

namespace HystericsLib.Avalonia.Dialogs
{
    [ResolvesTo(typeof(MessageDialogView))]
    public class MessageDialogViewModel : DialogViewModel<Boolean>
    {
		private String _caption = "";
        public String Caption
		{
			get => _caption;
			set => Set(ref _caption, value);
		}

        private String _okText = "OK";
        public String OkText
        {
            get => _okText;
            set => Set(ref _okText, value);
        }

        private String _cancelText = "Cancel";
        public String CancelText
        {
            get => _cancelText;
            set => Set(ref _cancelText, value);
        }

        private Boolean _showOk = true;
        public Boolean ShowOk
        {
            get => _showOk;
            set => Set(ref _showOk, value);
        }

        private Boolean _showCancel = true;
        public Boolean ShowCancel
        {
            get => _showCancel;
            set => Set(ref _showCancel, value);
        }

        public MessageDialogViewModel()
        {
            Title = "Message";
        }

        protected override Boolean OnComplete(Object? context) => (context is Boolean b) ? b : false;
    }
}
