using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace HystericsLib.Avalonia.Dialogs
{
    public class StringPromptDialogView : UserControl
    {
        public StringPromptDialogView()
        {
            this.InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
