using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

using HystericsLib;

namespace HystericsLib.Avalonia.Dialogs
{
    [ResolvesTo(typeof(SelectorDialogView))]
    public class SelectorDialogViewModel<T> : DialogViewModel<T> where T : notnull
    {
        public ObservableCollection<T> Options { get; }
		
		private String _caption = "";
        public String Caption
		{
			get => _caption;
			set => Set(ref _caption, value);
		}

        private T? _selectedOption = default;
        public T? SelectedOption
        {
             get => _selectedOption;
             set => Set(ref _selectedOption, value);
        }

        private String _okText = "OK";
        public String OkText
        {
            get => _okText;
            set => Set(ref _okText, value);
        }

        private String _cancelText = "Cancel";
        public String CancelText
        {
            get => _cancelText;
            set => Set(ref _cancelText, value);
        }

        private Boolean _showOk = true;
        public Boolean ShowOk
        {
            get => _showOk;
            set => Set(ref _showOk, value);
        }

        private Boolean _showCancel = true;
        public Boolean ShowCancel
        {
            get => _showCancel;
            set => Set(ref _showCancel, value);
        }

        public SelectorDialogViewModel(ObservableCollection<T> options)
        {
            Options = options;
            Fallback = default;
        }

        protected override T OnComplete(Object? context) => SelectedOption ?? throw new NullReferenceException(nameof(SelectedOption) + " is null.");

        protected override Boolean CanComplete(Object? context) => SelectedOption != null;
    }
}
