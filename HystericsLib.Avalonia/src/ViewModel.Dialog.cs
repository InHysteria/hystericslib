using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

using Avalonia;
using Avalonia.Data;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Threading;

using HystericsLib;

namespace HystericsLib.Avalonia
{
    public interface IDialogViewModel<TResult> where TResult : notnull
    {
        String Title { get; set; }

        void Activate() { }
        void Close();

        event Action<TResult?> Completed;
        event Action Cancelled;
    }
	
    //I'm aware this class blends the View and ViewModel.
    //I dont particularly care right now, if needs be in the future I can disentangle them.
    public abstract class DialogViewModel<TResult> : ViewModel, IDialogViewModel<TResult> where TResult : notnull
    {
        public event Action<TResult?> Completed = delegate { };
        public event Action Cancelled = delegate { };

        private String _title;
        public String Title
        {
            get => _title;
            set => Set(ref _title, value);
        }

        private Boolean _isActive = false;
        public Boolean IsActive
        {
            get => _isActive;
            protected set => Set(ref _isActive, value);
        }

        private SystemDecorations _SystemDecorations = SystemDecorations.Full;
        public SystemDecorations SystemDecorations
        {
            get => _SystemDecorations;
            protected set => Set(ref _SystemDecorations, value);
        }

        private Boolean _fallbackSet = false;
        private TResult? _fallback = default;
        public TResult? Fallback
        {
            get => _fallback;
            set
            {
                _fallbackSet = true;
                Set(ref _fallback, value);
            }
        }

        public LambdaCommand Complete { get; }
        public LambdaCommand Cancel { get; }

        public DialogViewModel()
        {
            _title = GetType().Name;

            Complete = new LambdaCommand(o => Completed(OnComplete(o)), CanComplete);
            Cancel = new LambdaCommand(o => Close(), CanCancel);
        }

        protected abstract TResult OnComplete(Object? context);

        protected virtual Boolean CanComplete(Object? context) => true;
        protected virtual Boolean CanCancel(Object? context) => true;

        public void Close()
        {
            if (_fallbackSet)
                Completed(Fallback);
            else
                Cancelled(); //Throws exception
        }
    }
}
