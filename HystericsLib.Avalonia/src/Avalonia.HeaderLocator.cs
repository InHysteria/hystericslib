using System;
using System.Linq;
using System.Reflection;

using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Templates;

namespace HystericsLib.Avalonia
{
    public class HeaderLocator : IDataTemplate
    {
        public Boolean SupportsRecycling => false;
        public Control Build(Object? data) => Resolve(data);

        public Control Resolve(Object? data)
        {
            if (data == null)
                throw new NullReferenceException("Cannot resolve <NULL>.");

            Type headerType = _getResolutionFor(data)
                ?.HeaderType
                ?? throw new Exception($"{data?.GetType()?.Name ?? "<NULL>"} cannot be used as a view model, it must be decorated with the {nameof(ResolvesTo)} attribute");

            Object? header = Activator.CreateInstance(headerType);
            if (header is Control cHeader)
            {
                if (header is StyledElement styled)
                    styled.DataContext = data;
				if (data is IInitialize initialize)
					initialize.OnInitialized();
                return cHeader;
            }

            throw new Exception($"{headerType.Name} cannot be used as a view. It must be assignable to {nameof(Control)}");
        }

        public Boolean Match(Object? data) => _getResolutionFor(data) != null;
		
        private ResolvesTo? _getResolutionFor(Object? obj) => obj
            ?.GetType()
            ?.GetCustomAttribute<ResolvesTo>();
    }
}
