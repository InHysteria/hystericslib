using System;
using System.Windows.Input;
using System.Threading.Tasks;

namespace HystericsLib.Avalonia
{
    public class LambdaCommand<T> : LambdaCommand
    {
        public LambdaCommand(Action<T> execute, Func<T, Boolean>? canExecute = null) : base(
            (parameter) => execute((T)parameter!),
            (parameter) => parameter is T tParameter && (canExecute?.Invoke(tParameter) ?? true)
        ) {}
    }

    public class LambdaCommand : ICommand
    {
        public static LambdaCommand Empty { get; } = new LambdaCommand(o => { });
        public event EventHandler? CanExecuteChanged = delegate { };
        public void RaiseCanExecuteChanged() => CanExecuteChanged!(this, new EventArgs());

        protected Action<Object?> _execute { get; set; }
        protected Func<Object?, Boolean> _canExecute { get; set; }

        public LambdaCommand(Action<Object?> execute, Func<Object?, Boolean>? canExecute = null)
        {
            _execute = execute;
            _canExecute = canExecute ?? ((parameter) => true);
        }

        public Boolean CanExecute(Object? parameter) => _canExecute(parameter);
        public void Execute(Object? parameter) => _execute(parameter);
    }
}
