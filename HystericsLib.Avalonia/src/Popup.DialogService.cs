using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

using Avalonia;
using Avalonia.Data;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Platform;
using Avalonia.Threading;

namespace HystericsLib.Avalonia
{
    public interface IDialogService
    {
        Boolean SupportsModal { get; }
        Boolean SupportsSystemDecorations { get; }

        Task<TResult?> Show<TResult>(IDialogViewModel<TResult> dialog) where TResult : notnull;
        Task<TResult?> Show<TResult>(IDialogViewModel<TResult> dialog, Boolean modal) where TResult : notnull;
    }
	
    public class PopupDialogService : IDialogService
    {
        private readonly IRuntime _application;

        public PopupDialogService(IRuntime application) => _application = application;

        public Boolean SupportsModal => true;
        public Boolean SupportsSystemDecorations => true;

        public Task<TResult?> Show<TResult>(IDialogViewModel<TResult> dialog) where TResult : notnull => Show(dialog, true);
        public async Task<TResult?> Show<TResult>(IDialogViewModel<TResult> dialog, Boolean modal) where TResult : notnull
        {
            using (Context<TResult> context = new Context<TResult>(dialog, modal ? _application.Window : null))
                return await context.Show();
        }


        class Context<T> : IDisposable where T : notnull
        {
            private IDialogViewModel<T> _dialog { get; }
            private Window? _parent { get; }
            private TaskCompletionSource<T?> _source { get; }
            private Window _window { get; }

            private Boolean _disposed = false;

            public Context(IDialogViewModel<T> dialog, Window? parent)
            {
                _dialog = dialog;
                _parent = parent;
                _source = new();
                _window = new();

                Panel wrapper = new();
                _window.Content = dialog;
                _window.DataContext = dialog;
                _window.CanResize = false;

                _window.SizeToContent = SizeToContent.WidthAndHeight;
                _window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                _window.Bind(Window.TitleProperty, new Binding("Title"));
                _window.Bind(Window.SystemDecorationsProperty, new Binding("SystemDecorations"));

                _window.Closed += _closed;
                _dialog.Completed += _complete;
                _dialog.Cancelled += _cancel;
            }

            public Task<T?> Show()
            {
                if (_disposed) throw new Exception("Context has been disposed.");

                if (_parent == null)
                    _window.Show();
                else
                    _window.ShowDialog(_parent);

                return _source.Task;
            }

            private void _complete(T? result) => _source.TrySetResult(result);
            private void _cancel()
            {
                try
                {
                    if (_source.Task.Status == TaskStatus.Canceled ||
                        _source.Task.Status == TaskStatus.RanToCompletion ||
                        _source.Task.Status == TaskStatus.Faulted)
                        return;

                    _dialog.Close();
                    if (_source.Task.Status != TaskStatus.RanToCompletion && !_source.Task.Wait(1000))
                    {
                        Console.WriteLine($"Context<{_dialog.GetType().Name}>: Graceful cancel took longer than 1s, cancelling task.");
                        _source?.TrySetCanceled();
                    }
                }
                catch
                {
                    _source?.TrySetCanceled();
                    throw;
                }
            }
            private void _closed(Object? source, EventArgs e) =>  _cancel();


            public void Dispose()
            {
                if (_disposed) return;

                _cancel();
                _dialog.Cancelled -= _cancel;
                _dialog.Completed -= _complete;
                _window.Closed -= _closed;
                _window.Close();
                _disposed = true;
            }
        }
    }
}
