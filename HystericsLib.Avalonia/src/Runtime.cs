namespace HystericsLib.Avalonia
{
	public interface IRuntime 
	{
		Window? Window { get; }
	}
	
	public class Runtime : Application, IRuntime
	{		
		public Window? Window { get; private set; }
		
		private KeyGesture? _devToolsGesture { get; }
		private IEnumerable<Uri> _styleIncludes { get; }
		private IEnumerable<Lazy<IDataTemplate>> _templates { get; }
		private IEnumerable<Lazy<IStyle>> _styles { get; }
		private Lazy<IMainViewModel> _mainViewModel { get; }
		
		public Runtime(
			Boolean darkTheme,
			KeyGesture? devToolsGesture,
			IEnumerable<Uri> styleIncludes,
			IEnumerable<Lazy<IDataTemplate>> templates,
			IEnumerable<Lazy<IStyle>> styles,
			Lazy<IMainViewModel> mainViewModel) => 
			(_templates, _styles, _mainViewModel, _devToolsGesture, _styleIncludes, RequestedThemeVariant) = 
			( templates,  styles,  mainViewModel,  devToolsGesture,  styleIncludes, darkTheme ? ThemeVariant.Dark : ThemeVariant.Light);
		
        public override void Initialize()
		{
			Styles.AddRange(_styleIncludes.Select(u => new StyleInclude(u) { Source = u }));

			DataTemplates.AddRange(_templates.Select(l => l.Value));
			Styles.AddRange(_styles.Select(l => l.Value));
		}
		
		public override void OnFrameworkInitializationCompleted()
		{					
			IMainViewModel mainViewModel = _mainViewModel.Value;
				  
			if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
				desktop.MainWindow = Window = 
					_devToolsGesture != null
					? new DevWindow(_devToolsGesture)
					{
						DataContext = mainViewModel,
						Content = mainViewModel,
						Title = mainViewModel.Title,
						Width = mainViewModel.Width,
						Height = mainViewModel.Height
					}
					: new Window
					{
						DataContext = mainViewModel,
						Content = mainViewModel,
						Title = mainViewModel.Title,
						Width = mainViewModel.Width,
						Height = mainViewModel.Height
					};
			else if (ApplicationLifetime is ISingleViewApplicationLifetime view)
				view.MainView = new ContentControl
				{
					DataContext = _mainViewModel.Value
				};				
				
			if (mainViewModel is IInitialize mainInit)	
				mainInit.OnInitialized();
			
			base.OnFrameworkInitializationCompleted();
		}
	}
}