using System;

using Avalonia;
using Avalonia.Controls;

namespace HystericsLib.Avalonia
{
    [AttributeUsage(AttributeTargets.Class, Inherited = true)]
    public class ResolvesTo : Attribute
    {
        public Type HeaderType { get; }
        public Type ViewType { get; }

        public ResolvesTo(Type type)
        {
            if (!typeof(Control).IsAssignableFrom(type)) throw new ArgumentException("Type must be able to be cast to Control.", nameof(type));
			HeaderType = type;
            ViewType = type;
        }

        public ResolvesTo(Type headerType, Type viewType)
        {
            if (!typeof(Control).IsAssignableFrom(headerType)) throw new ArgumentException("Type must be able to be cast to Control.", nameof(headerType));
            if (!typeof(Control).IsAssignableFrom(viewType)) throw new ArgumentException("Type must be able to be cast to Control.", nameof(viewType));
			HeaderType = headerType;
            ViewType = viewType;
        }
    }
}
