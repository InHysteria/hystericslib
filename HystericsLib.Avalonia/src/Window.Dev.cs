namespace HystericsLib.Avalonia
{
	public class DevWindow : Window
	{
		public KeyGesture DevToolsGesture { get; }

		public DevWindow(KeyGesture devToolsGesture) => DevToolsGesture = devToolsGesture;

		public void InitializeComponent()
		{
            this.AttachDevTools(DevToolsGesture);
		}
	}
}