using System;

namespace HystericsLib.Avalonia
{
    [AttributeUsage(AttributeTargets.Property)]
    public class NotifyAfter : Attribute
    {
        public String[] Members;

        public NotifyAfter(params String[] members) => Members = members;
    }
}
