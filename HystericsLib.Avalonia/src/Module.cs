namespace HystericsLib.Avalonia
{
	public class Module : Autofac.Module
	{		
		private Boolean _darkTheme { get; }
		private KeyGesture? _devToolsGesture { get; }
		
		public Module(Boolean DarkTheme = true, KeyGesture? DevToolsGesture = null) => (_darkTheme, _devToolsGesture) = (DarkTheme, DevToolsGesture);
		
		protected override void Load(ContainerBuilder builder)
		{
			builder.Register(scope => AppBuilder.Configure<Runtime>(scope.Resolve<Func<Runtime>>())).SingleInstance();
			builder.RegisterType<Runtime>()
				   .WithParameter(TypedParameter.From(_darkTheme))
				   .WithParameter(TypedParameter.From(_devToolsGesture))
				   .As<Application>()
				   .As<IRuntime>()
				   .AsSelf()
				   .SingleInstance();			
			
			builder.RegisterType<ViewLocator>().As<IDataTemplate>().AsSelf().InstancePerLifetimeScope();				
				
			builder.RegisterType<PopupDialogService>().As<IDialogService>().AsSelf().InstancePerLifetimeScope();
			builder.RegisterType<Dialogs.MessageDialogViewModel>().AsSelf();
			builder.RegisterGeneric(typeof(Dialogs.SelectorDialogViewModel<>)).AsSelf();
			builder.RegisterType<Dialogs.StringPromptDialogViewModel>().AsSelf();
			
			builder.RegisterType<Controls.MenuViewModel>().AsSelf();
			builder.RegisterType<Controls.MenuItemViewModel>().AsSelf();
			builder.RegisterType<Controls.TreeContentViewModel>().AsSelf();
		}		
	}
}
		