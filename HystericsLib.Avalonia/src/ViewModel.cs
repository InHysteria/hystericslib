using System;
using System.Linq;
using System.Reflection;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HystericsLib.Avalonia
{
	public interface IMainViewModel
	{
		String Title { get; }
		
		Int32 Width { get; }
		Int32 Height { get; }
	}
	
	public interface IInitialize
	{
		void OnInitialized();
	}
	
    public class ViewModel : INotifyPropertyChanged, INotifyDataErrorInfo
    {
        public event PropertyChangedEventHandler? PropertyChanged = delegate { };

        protected Boolean Set<T>(ref T field, T value, IEnumerable<String>? extraNames = null, IEnumerable<LambdaCommand>? relatedCommands = null, [CallerMemberName]String callerName = "")
        {
            if (Equals(field, value)) return false;
            field = value;
            RaisePropertyChanged(callerName);

            foreach (String name in extraNames ?? Enumerable.Empty<String>())
                RaisePropertyChanged(name);

            foreach (LambdaCommand command in relatedCommands ?? Enumerable.Empty<LambdaCommand>())
                command.RaiseCanExecuteChanged();

            return true;
        }
        protected Boolean Set<T>(Func<T> getter, Action<T> setter, T value, IEnumerable<String>? extraNames = null, IEnumerable<LambdaCommand>? relatedCommands = null, [CallerMemberName]String callerName = "")
        {
            if (Equals(getter(), value)) return false;
            setter(value);
            RaisePropertyChanged(callerName);

            foreach (String name in extraNames ?? Enumerable.Empty<String>())
                RaisePropertyChanged(name);

            foreach (LambdaCommand command in relatedCommands ?? Enumerable.Empty<LambdaCommand>())
                command.RaiseCanExecuteChanged();

            return true;
        }

        protected void RaisePropertyChanged([CallerMemberName]String name = "") => _raisePropertyChanged(name, new HashSet<String>());
        private void _raisePropertyChanged(String name, HashSet<String> ignore)
        {
            if (ignore.Contains(name)) return;
            ignore.Add(name);

            PropertyChanged!(this, new PropertyChangedEventArgs(name));
            if (_propogatingProperties.ContainsKey(name))
                foreach (String propogation in _propogatingProperties[name])
                    _raisePropertyChanged(propogation, ignore);
        }


		//INotifyDataErrorInfo
		public event EventHandler<DataErrorsChangedEventArgs>? ErrorsChanged = delegate { };
		private Boolean _hasErrors;
		public Boolean HasErrors 
		{
			get => _hasErrors;
			private set => Set(ref _hasErrors, value);
		}

		private Dictionary<String, List<String>> _errors { get; } = new();
		protected void NoErrors([CallerMemberName]String property = "") => _setErrors(property, Enumerable.Empty<String>());
		protected void SetError(String error, [CallerMemberName]String property = "") => _setErrors(property, new[] { error });
		protected void SetErrors(IEnumerable<String> errors, [CallerMemberName]String property = "") => _setErrors(property, errors);

		private void _setErrors(String property, IEnumerable<String> errors)
		{
			if (!_errors.ContainsKey(property))
				_errors[property] = new List<String>();

			_errors[property].Clear();
			_errors[property].AddRange(errors);

			HasErrors = _errors.Any(x => x.Value.Count > 0);

			ErrorsChanged!(this, new DataErrorsChangedEventArgs(property));
		}

		public System.Collections.IEnumerable GetErrors(String? propertyName) => propertyName != null && _errors.ContainsKey(propertyName)
			? _errors[propertyName]
			: Enumerable.Empty<String>();

        //Propogation Support
        private static Dictionary<Type, Dictionary<String, String[]>> _propogationCache = new();
        private Dictionary<String, String[]>? _propogatingPropertiesLazy = null;
        private Dictionary<String, String[]>  _propogatingProperties =>
            _propogatingPropertiesLazy ??= _propogationCache.ContainsKey(GetType())
                ? _propogationCache[GetType()]
                : _propogationCache[GetType()] = GetType()
                    .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                    .Select(m => (
                        member: m,
                        attribute: m.GetCustomAttribute<NotifyAfter>()))
                    .Where(t => t.attribute != null)
                    //IEnumerable<(MemberInfo member, UpdatesAfter attribute)>
                    .SelectMany(t => t
                        .attribute!
                        .Members
                        .Select(s => (
                            when: s,
                            trigger: t.member.Name)))
                    //IEnumerable<(String when, String trigger)>
                    .GroupBy(t => t.when, t => t.trigger)
                    .ToDictionary(
                        g => g.Key,
                        g => g.ToArray());
    }
}
