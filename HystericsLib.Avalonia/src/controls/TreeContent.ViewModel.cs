using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace HystericsLib.Avalonia.Controls
{	
	public interface ITreeNode 
	{
		ITreeNode? Parent { get; set; }
		IList<ITreeNode> Children { get; }
	}

    [ResolvesTo(typeof(TreeContentView))]
    public class TreeContentViewModel : ViewModel, ITreeNode
    {
		private ITreeNode? _selected = null;
		public ITreeNode? Selected
		{
			get => _selected;
			set => Set(ref _selected, value);
		}

		public IList<ITreeNode> Children { get; init; } = new ObservableCollection<ITreeNode>();

		ITreeNode? ITreeNode.Parent
		{
			get => null;
			set => throw new NotSupportedException($"{nameof(TreeContentViewModel)} is a root node.");
		}
    }
}
