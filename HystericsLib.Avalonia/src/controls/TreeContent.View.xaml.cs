using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace HystericsLib.Avalonia.Controls
{
    public class TreeContentView : UserControl
    {
		public static readonly StyledProperty<Boolean> DraggableProperty =
			AvaloniaProperty.Register<TreeContentView, Boolean>(nameof(Draggable), defaultValue: false);

		public Boolean Draggable
		{
			get => GetValue(DraggableProperty);
			set => SetValue(DraggableProperty, value);
		}

        public TreeContentView()
        {
            this.InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
