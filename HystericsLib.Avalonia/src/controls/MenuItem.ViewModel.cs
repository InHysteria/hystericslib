
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

using HystericsLib;

namespace HystericsLib.Avalonia.Controls
{
    public class MenuItemViewModel
    {
        public static readonly MenuItemViewModel Separator = new MenuItemViewModel("-");

        public String Header { get; }
        public IReadOnlyList<MenuItemViewModel> Items { get; }
        public LambdaCommand? Command { get; init; }
		
		public virtual Boolean IsCheckable { get; } = false;

        public MenuItemViewModel(String header) : this(header, null, null) { }

        public MenuItemViewModel(
            String header,
            LambdaCommand? command = null,
            IEnumerable<MenuItemViewModel>? items = null)
        {
            Header = header;
            Command = command;
            Items = (items ?? Enumerable.Empty<MenuItemViewModel>())
                .ToList()
                .AsReadOnly();
        }
    }
}
