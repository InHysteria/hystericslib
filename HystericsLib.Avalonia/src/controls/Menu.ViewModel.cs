
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace HystericsLib.Avalonia.Controls
{
    public interface IContextual
    {
        MenuViewModel ContextMenu { get; }
    }
	
    [ResolvesTo(typeof(MenuView))]
    public class MenuViewModel : ViewModel
    {
        public ObservableCollection<MenuItemViewModel> Items { get; } = new();


        public FluentBuilder Start() => new FluentBuilder(this);
        public class FluentBuilder
        {
            private MenuViewModel _menu { get; }
            private Stack<(String header, List<MenuItemViewModel> items)> _buildStack = new();

            internal FluentBuilder(MenuViewModel menu)
            {
                _menu = menu;
                _buildStack.Push(("__ROOT", new()));
            }

            public FluentBuilder StartMenu(String header)
            {
                _buildStack.Push((header, new()));
                return this;
            }


            public FluentBuilder AddSeparator()
            {
                _buildStack
                    .Peek()
                    .items
                    .Add(new MenuItemViewModel("-"));
                return this;
            }
            public FluentBuilder AddEntry(String header, Action<Object?> command, Func<Object?, Boolean>? condition = null) => AddEntry(header, new LambdaCommand(command, condition));
            public FluentBuilder AddEntry(String header, LambdaCommand command)
            {
                _buildStack
                    .Peek()
                    .items
                    .Add(new MenuItemViewModel(header, command, null));
                return this;
            }
			public FluentBuilder AddToggle(String header, Func<Boolean> getter, Action<Boolean> setter)
			{
				_buildStack
					.Peek()
					.items
					.Add(new ToggleMenuItemViewModel(header, getter, setter));
				return this;
			}
            public FluentBuilder AddMenu(String header, IEnumerable<MenuItemViewModel> items)
            {
                _buildStack
                    .Peek()
                    .items
                    .Add(new MenuItemViewModel(header, null, items));
                return this;
            }
            public FluentBuilder AddMenus(IEnumerable<MenuItemViewModel> items)
            {
                _buildStack
                    .Peek()
                    .items
                    .AddRange(items);
                return this;
            }

            public FluentBuilder EndMenu()
            {
                if (_buildStack.Count <= 1) throw new Exception("Not in a menu.");

                (String header, List<MenuItemViewModel> items) = _buildStack.Pop();
                AddMenu(header, items);

                return this;
            }

            public MenuViewModel End()
            {
                if (_buildStack.Count > 1) throw new Exception("There are unfinished menus.");

                foreach (MenuItemViewModel itemVm in _buildStack.Pop().items)
                    _menu.Items.Add(itemVm);

                return _menu;
            }
        }
    }
}
