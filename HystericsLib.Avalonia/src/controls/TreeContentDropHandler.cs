using Avalonia.VisualTree;
using Avalonia.Xaml.Interactions.DragAndDrop;

namespace HystericsLib.Avalonia.Controls;

public class TreeContentDropHandler : DropHandlerBase
{
	private Boolean Plan(Object? sender, DragEventArgs e, Object? sourceContext, Boolean perform)
	{
		if (e.Source is Control &&
			e.DragEffects is DragDropEffects.Move &&
			sender is TreeView treeView &&			
			sourceContext is ITreeNode sourceNode &&
			treeView.GetVisualAt(e.GetPosition(treeView)) is StyledElement targetControl &&
			targetControl.DataContext is ITreeNode targetNode &&			
			sourceNode.Parent is ITreeNode sourceParent &&			
			targetNode.Parent is ITreeNode targetParent)
		{
			Int32 sourceIndex = sourceParent.Children.IndexOf(sourceNode);
			Int32 targetIndex = targetParent.Children.IndexOf(targetNode);

			if (sourceIndex < 0 && targetIndex < 0)
				return false;
			
			if (perform)	
			{
				if (sourceParent != targetParent)
				{
					sourceNode.Parent = targetParent;
					MoveItem(
						sourceParent.Children, targetParent.Children,
						sourceIndex, 		   targetIndex);
				}
				else
					MoveItem(
						sourceParent.Children, 
						sourceIndex, 		   targetIndex);						
			}

			return true;				
		}

		return false;
	}

	public override Boolean Validate(Object? sender, DragEventArgs e, Object? sourceContext, Object? targetContext, Object? _) => 
		Plan(sender, e, sourceContext, false);

	public override Boolean Execute(Object? sender, DragEventArgs e, Object? sourceContext, Object? targetContext, Object? _) => 
		Plan(sender, e, sourceContext, true);
}