
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

using HystericsLib;

namespace HystericsLib.Avalonia.Controls
{
    public class ToggleMenuItemViewModel : MenuItemViewModel, INotifyPropertyChanged
    {
		private Func<Boolean> _getter { get; }
		private Action<Boolean> _setter { get; }
		
        public event PropertyChangedEventHandler? PropertyChanged = delegate { };
		
		public override Boolean IsCheckable { get; } = true;
		
		public Boolean IsChecked 
		{
			get => _getter();
			set
			{
				_setter(value);
				
				PropertyChanged!(this, new PropertyChangedEventArgs(nameof(IsChecked)));
			}
		}
				
        public ToggleMenuItemViewModel(String header, Func<Boolean> getter, Action<Boolean> setter) : base(header, null, null)
        {			
			_getter = getter;
			_setter = setter;
			
			Command = new LambdaCommand(o => IsChecked = !IsChecked);
        }
    }
}
