using System;
using System.Linq;
using System.Reflection;

using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Templates;

namespace HystericsLib.Avalonia
{
    public class ViewLocator : IDataTemplate
    {
        public Boolean SupportsRecycling => false;
        public Control Build(Object? data) => Resolve(data);

        public Control Resolve(Object? data)
        {
            if (data == null)
                throw new NullReferenceException("Cannot resolve <NULL>.");

            Type viewType = _getResolutionFor(data)
                ?.ViewType
                ?? throw new Exception($"{data?.GetType()?.Name ?? "<NULL>"} cannot be used as a view model, it must be decorated with the {nameof(ResolvesTo)} attribute");

            Object? view = Activator.CreateInstance(viewType);
            if (view is Control cView)
            {
                if (view is StyledElement styled)
                    styled.DataContext = data;
				if (data is IInitialize initialize)
					initialize.OnInitialized();
                return cView;
            }

            throw new Exception($"{viewType.Name} cannot be used as a view. It must be assignable to {nameof(Control)}");
        }

        public Boolean Match(Object? data) => _getResolutionFor(data) != null;
		
        private ResolvesTo? _getResolutionFor(Object? obj) => obj
            ?.GetType()
            ?.GetCustomAttribute<ResolvesTo>();
    }
}
