using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics.CodeAnalysis;
using System.ComponentModel;

namespace HystericsLib.Contracts
{
    public partial class ConstantContract<T> : ContractBase<T>
    {
        private readonly T _value;
        public override T? Value
        {
            get => _value;
            set => throw new NotSupportedException("Cannot modify a ConstantContract, consider using a model instead.");
        }

        public ConstantContract(T value)
        {
            _value = value;
        }
    }
}
