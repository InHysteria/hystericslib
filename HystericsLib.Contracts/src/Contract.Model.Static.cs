using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics.CodeAnalysis;
using System.ComponentModel;

namespace HystericsLib.Contracts
{
    public static class ModelContract
    {
        //Sugar to make syntax lighter.
        //Allows ModelContract.For(<Model>, m => m.<Member>)
        public static ModelContract<TField> For<TModel, TField>(TModel model, Expression<Func<TModel, TField>> expression)
            where TModel : class =>
                ModelContract<TField>.For<TModel>(model, expression);

        public static void Set<TModel, TField>(TModel model, Expression<Func<TModel, TField>> expression, TField value)
            where TModel : class =>
                ModelContract<TField>.For<TModel>(model, expression).Value = value;
    }

    public partial class ModelContract<T>
    {
        private static Dictionary<(Object, MemberInfo), ModelContract<T>> _cache { get; } = new();

        public static ModelContract<T> For<TModel>(TModel model, Expression<Func<TModel, T>> expression) where TModel : class
        {
            MemberInfo member = expression.Body is MemberExpression mexp
                ? mexp.Member
                : throw new ArgumentException("Expression is not a member access", "expression");

            (Object, MemberInfo) key = ((Object)model, member);

            return _cache.ContainsKey(key)
                ? _cache[key]
                : _cache[key] = member switch
                {
                    PropertyInfo property   => new ModelContract<T>(model, property),
                    FieldInfo field         => new ModelContract<T>(model, field),

                    _ => throw new ArgumentException("Expression points to a member which is not a property or a field.", "expression")
                };
        }
    }
}
