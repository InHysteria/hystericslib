using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace HystericsLib.Contracts
{
    public interface ICollectionContract<T> : ICollection<T>, INotifyCollectionChanged, IDisposable
    {
        void Refresh();
    }
}
