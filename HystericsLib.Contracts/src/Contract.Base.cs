using System;
using System.ComponentModel;

namespace HystericsLib.Contracts
{
    public abstract class ContractBase<T> : IContract<T>
    {
        public virtual event PropertyChangedEventHandler? PropertyChanged = delegate { };

        public abstract T? Value { get; set; }

        public Object? GetValueBoxed() => (Object?)Value;

        public virtual void Dispose() { }
        public virtual void RaisePropertyChanged() => PropertyChanged!(this, new PropertyChangedEventArgs(nameof(Value)));

        public override String? ToString() => Value?.ToString();
    }
}
