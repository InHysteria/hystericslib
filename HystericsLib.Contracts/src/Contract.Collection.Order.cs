using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics.CodeAnalysis;
using System.ComponentModel;

namespace HystericsLib.Contracts
{
    public static class OrderCollectionContract
    {
        public static OrderCollectionContract<TSource,TKey> OrderBy<TSource,TKey>(
            this ICollectionContract<TSource> source,
            Func<TSource,TKey> keySelector,
            IComparer<TKey>? comparer = null) => new OrderCollectionContract<TSource, TKey>(source, keySelector, comparer);

        public static OrderCollectionContract<TSource,TKey> OrderByDescending<TSource,TKey>(
            this ICollectionContract<TSource> source,
            Func<TSource,TKey> keySelector,
            IComparer<TKey>? comparer = null) => new OrderCollectionContract<TSource, TKey>(source, keySelector, comparer, true);
    }

    public class OrderCollectionContract<TSource, TKey> : ICollectionContract<TSource>
    {
        private readonly ICollectionContract<TSource> _source;
        private readonly Func<TSource, TKey> _keySelector;
        private readonly IComparer<TKey> _comparer;
        private readonly Boolean _descending;

        private Int32[] _map = new Int32[0];

        public OrderCollectionContract(ICollectionContract<TSource> source, Func<TSource, TKey> keySelector, IComparer<TKey>? comparer = null, Boolean descending = false)
        {
            _source = source;
            _keySelector = keySelector;
            _comparer = comparer ?? Comparer<TKey>.Default;
            _descending = descending;

            if (_source.Count != 0)
                _map = Sort(_source, _source.Count);
        }

        //Sorting. Adapted from..
        //  https://referencesource.microsoft.com/#System.Core/System/Linq/Enumerable.cs,2590
        //  https://referencesource.microsoft.com/#System.Core/System/Linq/Enumerable.cs,2633
        private TKey[] _keys = new TKey[0];

        private void ComputeKeys(ICollection<TSource> elements, int count)
        {
            _keys = elements
                .Select(_keySelector)
                .ToArray();
        }

        private int CompareKeys(int index1, int index2)
        {
            int c = _comparer.Compare(_keys[index1], _keys[index2]);
            if (c == 0)
                return index1 - index2;
            return _descending ? -c : c;
        }

        private int[] Sort(ICollection<TSource> elements, int count)
        {
            ComputeKeys(elements, count);
            int[] map = new int[count];
            for (int i = 0; i < count; i++)
                map[i] = i;
            QuickSort(map, 0, count - 1);
            return map;
        }

        private void QuickSort(int[] map, int left, int right)
        {
            do {
                int i = left;
                int j = right;
                int x = map[i + ((j - i) >> 1)];
                do {
                    while (i < map.Length && CompareKeys(x, map[i]) > 0) i++;
                    while (j >= 0 && CompareKeys(x, map[j]) < 0) j--;
                    if (i > j) break;
                    if (i < j) {
                        int temp = map[i];
                        map[i] = map[j];
                        map[j] = temp;
                    }
                    i++;
                    j--;
                } while (i <= j);
                if (j - left <= right - i) {
                    if (left < j) QuickSort(map, left, j);
                    left = i;
                }
                else {
                    if (i < right) QuickSort(map, i, right);
                    right = j;
                }
            } while (left < right);
        }



        //Wrapping.
        public Int32 Count => _source.Count;
        public Boolean IsReadOnly => true; //_source.IsReadOnly;

        public void Add(TSource item) => throw new NotSupportedException("Cannot modify ordered collections."); //_source.Add(item);
        public Boolean Remove(TSource item) => throw new NotSupportedException("Cannot modify ordered collections."); //_source.Remove(item);
        public void Clear() => throw new NotSupportedException("Cannot modify ordered collections."); //_source.Clear();

        public Boolean Contains(TSource item) => this.Any(x => Object.Equals(x, item));
        public void CopyTo(TSource[] array, Int32 arrayIndex) => this.ToList().CopyTo(array, arrayIndex);

        public IEnumerator<TSource> GetEnumerator()
        {
            if (_source.Count <= 0) yield break;


            Int32 i = 0;
            List<TSource> buffer = new List<TSource>(_source);
            while (i < Count)
            {
                yield return buffer[_map[i]];
                i++;
            }
        }
        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

        public void Dispose() => _source.Dispose();



        //Event wrapping
        private event NotifyCollectionChangedEventHandler? _collectionChanged;
        private Int32 _attachedHandlers = 0;

        private void _attach() => _source.CollectionChanged += _onSourceCollectionChanged;
        private void _detatch() => _source.CollectionChanged -= _onSourceCollectionChanged;

        private void _onSourceCollectionChanged(Object? source, NotifyCollectionChangedEventArgs args)
        {
            Int32[] oldMap = _map;
            if (_source.Count == 0)
                _map = new Int32[0];
            else
                _map = Sort(_source, _source.Count);

            if (_collectionChanged == null) return;

            NotifyCollectionChangedEventArgs? newArgs = null;
            switch (args.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    //Case, One item
                    ////Case, Many items
                    newArgs = new NotifyCollectionChangedEventArgs(
                        args.Action,
                        args.NewItems,
                        _map[args.NewStartingIndex]
                    );
                    break;
                case NotifyCollectionChangedAction.Remove:
                    //Case, One item
                    //Case, Many items
                    newArgs = new NotifyCollectionChangedEventArgs(
                        args.Action,
                        args.OldItems,
                        oldMap[args.OldStartingIndex]
                    );
                    break;
                case NotifyCollectionChangedAction.Replace:
                    //Case, Two seperate items
                    //Case, Two lists of items
                    newArgs = new NotifyCollectionChangedEventArgs(
                        args.Action,
                        args.NewItems!,
                        args.OldItems!,
                        _map[args.NewStartingIndex]
                    );
                    break;
                case NotifyCollectionChangedAction.Move:
                    //Case, One item and two indices
                    //Case, Many items and two indices
                    newArgs = new NotifyCollectionChangedEventArgs(
                        args.Action,
                        args.NewItems,
                        _map[args.NewStartingIndex],
                        _map[args.OldStartingIndex]
                    );
                    break;
                case NotifyCollectionChangedAction.Reset:
                    newArgs = args;
                    break;
            }

            if (newArgs != null)
                _collectionChanged(this, newArgs);
        }

        public event NotifyCollectionChangedEventHandler? CollectionChanged
        {
            add
            {
                if (_attachedHandlers == 0)
                    _attach();
                _collectionChanged += value;
                _attachedHandlers++;
            }
            remove
            {
                _collectionChanged -= value;
                _attachedHandlers--;
                if (_attachedHandlers == 0)
                    _detatch();
            }
        }

        public void Refresh() => _source.Refresh();
    }
}
