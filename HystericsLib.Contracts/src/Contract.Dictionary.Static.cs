using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics.CodeAnalysis;
using System.ComponentModel;

namespace HystericsLib.Contracts
{
    public static class DictionaryContract
    {
        //Sugar to make syntax lighter.
        //Allows DictionaryContract.For(<Model>, m => m.<Member>)
        public static DictionaryContract<TKey, TValue> For<TModel, TKey, TValue>(TModel model, Expression<Func<TModel, IDictionary<TKey, TValue>>> expression)
            where TModel : class
            where TKey : notnull =>
                DictionaryContract<TKey, TValue>.For<TModel>(model, expression);

        public static DictionaryContract<TKey, TValue> For<TKey, TValue>(IDictionary<TKey, TValue> list)
            where TKey : notnull =>
                DictionaryContract<TKey, TValue>.For(list);

        public static DictionaryContract<TKey, TValue> AsContract<TKey, TValue>(this IDictionary<TKey, TValue> list)
            where TKey : notnull =>
                For<TKey, TValue>(list);
    }

    public partial class DictionaryContract<TKey, TValue>
    {
        private static Dictionary<(Object, MemberInfo), DictionaryContract<TKey, TValue>> _cache { get; } = new();

        public static DictionaryContract<TKey, TValue> For<TModel>(TModel model, Expression<Func<TModel, IDictionary<TKey, TValue>>> expression) where TModel : class
        {
            MemberInfo member = expression.Body is MemberExpression mexp
                ? mexp.Member
                : throw new ArgumentException("Expression is not a member access", "expression");

            (Object, MemberInfo) key = (model, member);

            return _cache.ContainsKey(key)
                ? _cache[key]
                : _cache[key] = member switch
                {
                    PropertyInfo property   => For((IDictionary<TKey, TValue>)(property.GetValue(model) ?? throw new NullReferenceException("CollectionContract cannot be created for a null list."))),
                    FieldInfo field         => For((IDictionary<TKey, TValue>)(field.GetValue(model) ?? throw new NullReferenceException("CollectionContract cannot be created for a null list."))),

                    _ => throw new ArgumentException("Expression points to a member which is not a property or a field.", "expression")
                };
        }

        public static DictionaryContract<TKey, TValue> For(IDictionary<TKey, TValue> dictionary) => new DictionaryContract<TKey, TValue>(dictionary);
    }
}
