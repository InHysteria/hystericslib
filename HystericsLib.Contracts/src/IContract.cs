using System;
using System.ComponentModel;

namespace HystericsLib.Contracts
{
    public interface IContract<T> : IContract, INotifyPropertyChanged, IDisposable
    {
        T? Value { get; set; }
    }

    public interface IContract
    {
        Object? GetValueBoxed();
        void RaisePropertyChanged();
    }
}
