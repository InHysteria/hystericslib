using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics.CodeAnalysis;
using System.ComponentModel;

namespace HystericsLib.Contracts
{
    public partial class ModelContract<T> : ContractBase<T>
    {
        private Object _model;
        private Func<Object?, Object?> _get;
        private Action<Object?, Object?> _set;

        public override T? Value
        {
            get => (T?)_get(_model);
            set
            {
                if (Object.Equals(value, Value)) return;

                _set(_model, value);
                RaisePropertyChanged();
            }
        }

        protected ModelContract(Object model, FieldInfo field)
        {
            if (field.FieldType != typeof(T)) throw new ArgumentException($"FieldInfo must refer to a field of type {typeof(T)}", nameof(field));

            _model = model;
            _get = field.GetValue;
            _set = field.SetValue;
        }
        protected ModelContract(Object model, PropertyInfo property)
        {
            if (property.PropertyType != typeof(T)) throw new ArgumentException($"PropertyInfo must refer to a property of type {typeof(T)}", nameof(property));

            _model = model;
            _get = property.GetValue;
            _set = property.SetValue;
        }
    }
}
