using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics.CodeAnalysis;
using System.ComponentModel;

namespace HystericsLib.Contracts
{
    public static class TransformCollectionContract
    {
        public static TransformCollectionContract<TSource, TProperty> Transform<TSource, TProperty>(
            this ICollectionContract<TSource> source,
            Func<TSource, TProperty> get,
            Func<TProperty, TSource>? set = null) => new TransformCollectionContract<TSource, TProperty>(source, get, set);
    }

    public class TransformCollectionContract<TSource, TProperty> : ICollectionContract<TProperty>
    {
        private readonly ICollectionContract<TSource> _source;
        private readonly Func<TSource, TProperty> _get;
        private readonly Func<TProperty, TSource>? _set;

        public Int32 Count => _source.Count;
        public Boolean IsReadOnly => _source.IsReadOnly;

        public TransformCollectionContract(ICollectionContract<TSource> source, Func<TSource, TProperty> get) : this(source, get, null) { }
        public TransformCollectionContract(ICollectionContract<TSource> source, Func<TSource, TProperty> get, Func<TProperty, TSource>? set)
        {
            _source = source;
            _get = get;
            _set = set;
        }

        private TSource _do_set(TProperty item)
        {
            if (_set == null) throw new InvalidOperationException("Cannot use this operation on a read only tranfsformation.");

            return _set(item);
        }

        public void Add(TProperty item) => _source.Add(_do_set(item));
        public Boolean Remove(TProperty item) => _source.Remove(_do_set(item));
        public void Clear()
        {
            if (_set == null) throw new InvalidOperationException("Cannot use this operation on a read only tranfsformation.");

            _source.Clear();
        }

        public Boolean Contains(TProperty item) => this.Any(x => Object.Equals(x, item));
        public void CopyTo(TProperty[] array, Int32 arrayIndex) => this.ToList().CopyTo(array, arrayIndex);

        public IEnumerator<TProperty> GetEnumerator() => _source.Select(_get).GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

        public void Dispose() => _source.Dispose();



        //Event wrapping
        private event NotifyCollectionChangedEventHandler? _collectionChanged;
        private Int32 _attachedHandlers = 0;

        private void _attach() => _source.CollectionChanged += _onSourceCollectionChanged;
        private void _detatch() => _source.CollectionChanged -= _onSourceCollectionChanged;

        private void _onSourceCollectionChanged(Object? source, NotifyCollectionChangedEventArgs args)
        {
            if (_collectionChanged == null) return;

            //This is the minimum complexity as there is no simple way to replicate the args.
            NotifyCollectionChangedEventArgs? newArgs = null;
            switch (args.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    //Case, One item
                    ////Case, Many items
                    newArgs = new NotifyCollectionChangedEventArgs(
                        args.Action,
                        args.NewItems!
                            .OfType<TSource>()
                            .Select(_get)
                            .ToList(),
                        args.NewStartingIndex
                    );
                    break;
                case NotifyCollectionChangedAction.Remove:
                    //Case, One item
                    //Case, Many items
                    newArgs = new NotifyCollectionChangedEventArgs(
                        args.Action,
                        args.OldItems!
                            .OfType<TSource>()
                            .Select(_get)
                            .ToList(),
                        args.OldStartingIndex
                    );
                    break;
                case NotifyCollectionChangedAction.Replace:
                    //Case, Two seperate items
                    //Case, Two lists of items
                    newArgs = new NotifyCollectionChangedEventArgs(
                        args.Action,
                        args.NewItems!
                            .OfType<TSource>()
                            .Select(_get)
                            .ToList(),
                        args.OldItems!
                            .OfType<TSource>()
                            .Select(_get)
                            .ToList(),
                        args.NewStartingIndex
                    );
                    break;
                case NotifyCollectionChangedAction.Move:
                    //Case, One item and two indices
                    //Case, Many items and two indices
                    newArgs = new NotifyCollectionChangedEventArgs(
                        args.Action,
                        args.NewItems!
                            .OfType<TSource>()
                            .Select(_get)
                            .ToList(),
                        args.NewStartingIndex,
                        args.OldStartingIndex
                    );
                    break;
                case NotifyCollectionChangedAction.Reset:
                    newArgs = args;
                    break;
            }

            if (newArgs != null)
                _collectionChanged(this, newArgs);
        }

        public event NotifyCollectionChangedEventHandler? CollectionChanged
        {
            add
            {
                if (_attachedHandlers == 0)
                    _attach();
                _collectionChanged += value;
                _attachedHandlers++;
            }
            remove
            {
                _collectionChanged -= value;
                _attachedHandlers--;
                if (_attachedHandlers == 0)
                    _detatch();
            }
        }

        public void Refresh() => _source.Refresh();
    }
}
