using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics.CodeAnalysis;
using System.ComponentModel;

namespace HystericsLib.Contracts
{
    public partial class DictionaryContract<TKey, TValue> : ICollectionContract<KeyValuePair<TKey,TValue>>, IDictionary<TKey, TValue> where TKey : notnull
    {
        private readonly IDictionary<TKey, TValue> _contents;

        public event NotifyCollectionChangedEventHandler? CollectionChanged = delegate { };

        public Int32 Count => _contents.Count;
        public Boolean IsReadOnly => _contents.IsReadOnly;

        public ICollection<TKey> Keys => _contents.Keys;
        public ICollection<TValue> Values => _contents.Values;

        public TValue this[TKey key]
        {
            get => _contents[key];
            set
            {
                if (ContainsKey(key))
                {
                    TValue oldValue = _contents[key];

                    _contents[key] = value;
                    _notifyCollectionChanged(NotifyCollectionChangedAction.Replace, key, value, oldValue);
                }
                else
                {
                    _contents[key] = value;
                    _notifyCollectionChanged(NotifyCollectionChangedAction.Add, key, value);
                }

            }
        }

        protected DictionaryContract(IDictionary<TKey, TValue> contents)
        {
            _contents = contents;
        }

        public void Add(TKey key, TValue value)
        {
            _contents.Add(key, value);
            _notifyCollectionChanged(NotifyCollectionChangedAction.Add, key, value);
        }
        public void Add(KeyValuePair<TKey, TValue> item)
        {
            _contents.Add(item);
            _notifyCollectionChanged(NotifyCollectionChangedAction.Add, item.Key, item.Value);
        }

        public Boolean Remove(TKey key)
        {
            TValue value = _contents[key];
            Boolean result = _contents.Remove(key);

            _notifyCollectionChanged(NotifyCollectionChangedAction.Remove, key, value);
            return result;
        }
        public Boolean Remove(KeyValuePair<TKey, TValue> item)
        {
            Boolean result = _contents.Remove(item);

            _notifyCollectionChanged(NotifyCollectionChangedAction.Remove, item.Key, item.Value);
            return result;
        }

        public void Clear()
        {
            _contents.Clear();
            _notifyCollectionChanged(NotifyCollectionChangedAction.Reset);
        }

        public Boolean ContainsKey(TKey key) => _contents.ContainsKey(key);
        public Boolean Contains(KeyValuePair<TKey, TValue> item) => _contents.Contains(item);
        public void CopyTo(KeyValuePair<TKey, TValue>[] array, Int32 arrayIndex) => _contents.CopyTo(array, arrayIndex);
        public Boolean TryGetValue(TKey key, [MaybeNullWhen(false)] out TValue value) => _contents.TryGetValue(key, out value);

        private void _notifyCollectionChanged(NotifyCollectionChangedAction action) => CollectionChanged!(this, new NotifyCollectionChangedEventArgs(action));
        private void _notifyCollectionChanged(NotifyCollectionChangedAction action, TKey key, TValue value) => CollectionChanged!(this, new NotifyCollectionChangedEventArgs(action, new KeyValuePair<TKey, TValue>(key, value)));
        private void _notifyCollectionChanged(NotifyCollectionChangedAction action, TKey key, TValue newValue, TValue oldValue) => CollectionChanged!(this, new NotifyCollectionChangedEventArgs(action, new KeyValuePair<TKey, TValue>(key, newValue), new KeyValuePair<TKey, TValue>(key, oldValue)));

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() => _contents.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

        public void Dispose() { }

        public void Refresh() => _notifyCollectionChanged(NotifyCollectionChangedAction.Reset);
    }
}
