using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics.CodeAnalysis;
using System.ComponentModel;

namespace HystericsLib.Contracts
{
    public static class CollectionContract
    {
        //Sugar to make syntax lighter.
        //Allows CollectionContract.For(<Model>, m => m.<Member>)
        public static CollectionContract<TField> For<TModel, TField>(TModel model, Expression<Func<TModel, IList<TField>>> expression)
            where TModel : class =>
                CollectionContract<TField>.For<TModel>(model, expression);

        public static CollectionContract<TField> For<TField>(IList<TField> list) =>
                CollectionContract<TField>.For(list);

        public static CollectionContract<TField> AsContract<TField>(this IList<TField> list) =>
                For<TField>(list);
    }

    public partial class CollectionContract<T>
    {
        private static Dictionary<(Object, MemberInfo), CollectionContract<T>> _cache { get; } = new();

        public static CollectionContract<T> For<TModel>(TModel model, Expression<Func<TModel, IList<T>>> expression) where TModel : class
        {
            MemberInfo member = expression.Body is MemberExpression mexp
                ? mexp.Member
                : throw new ArgumentException("Expression is not a member access", "expression");

            (Object, MemberInfo) key = (model, member);

            return _cache.ContainsKey(key)
                ? _cache[key]
                : _cache[key] = member switch
                {
                    PropertyInfo property   => For((IList<T>)(property.GetValue(model) ?? throw new NullReferenceException("CollectionContract cannot be created for a null list."))),
                    FieldInfo field         => For((IList<T>)(field.GetValue(model) ?? throw new NullReferenceException("CollectionContract cannot be created for a null list."))),

                    _ => throw new ArgumentException("Expression points to a member which is not a property or a field.", "expression")
                };
        }

        public static CollectionContract<T> For(IList<T> list) => new CollectionContract<T>(list);
    }
}
