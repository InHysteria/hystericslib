using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics.CodeAnalysis;
using System.ComponentModel;

namespace HystericsLib.Contracts
{
    public static class TransformContract
    {
        public static TransformContract<TSource, TProperty> Transform<TSource, TProperty>(
            this IContract<TSource> source,
            Func<TSource?, TProperty?> get,
            Func<TProperty?, TSource?>? set = null) => new TransformContract<TSource, TProperty>(source, get, set);
    }

    public class TransformContract<TSource, TProperty> : ContractBase<TProperty>
    {
        private readonly IContract<TSource> _source;
        private readonly Func<TSource?, TProperty?> _get;
        private readonly Func<TProperty?, TSource?>? _set;

        public override event PropertyChangedEventHandler? PropertyChanged
        {
            add => _source.PropertyChanged += value;
            remove => _source.PropertyChanged -= value;
        }

        public override TProperty? Value
        {
            get => _get(_source.Value);
            set
            {
                if (_set == null) throw new InvalidOperationException("Cannot set Value of readonly transformation.");

                _source.Value = _set(value);
            }
        }

        public TransformContract(IContract<TSource> source, Func<TSource?, TProperty?> get) : this(source, get, null) { }
        public TransformContract(IContract<TSource> source, Func<TSource?, TProperty?> get, Func<TProperty?, TSource?>? set)
        {
            _source = source;
            _get = get;
            _set = set;
        }

        public override void RaisePropertyChanged() => _source.RaisePropertyChanged();
    }
}
