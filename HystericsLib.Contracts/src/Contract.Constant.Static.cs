using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics.CodeAnalysis;
using System.ComponentModel;

namespace HystericsLib.Contracts
{
    public static class ConstantContract
    {
        //Sugar to make syntax lighter.
        //Allows ConstantContract.For(<Model>, m => m.<Member>)
        public static ConstantContract<T> For<T>(T value) => new ConstantContract<T>(value);
    }
}
