using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics.CodeAnalysis;
using System.ComponentModel;

namespace HystericsLib.Contracts
{

    /*CollectionContract
        .For(Data, d => d.Users) //CollectionContract<User> : ICollectionContract<IUser>
        .Transform(u => u.Username) //TransformCollectionContract<User, String> : ICollectionContract<String>
    */
    public partial class CollectionContract<T> : ICollectionContract<T>, IList<T>
    {
        private IList<T> _contents;

        public event NotifyCollectionChangedEventHandler? CollectionChanged = delegate { };

        public Int32 Count => _contents.Count;
        public Boolean IsReadOnly => _contents.IsReadOnly;

        public T this[Int32 index]
        {
            get => _contents[index];
            set
            {
                T oldValue = _contents[index];
                _contents[index] = value;

                _notifyCollectionChanged(NotifyCollectionChangedAction.Replace, value, oldValue, index);
            }
        }

        protected CollectionContract(IList<T> contents)
        {
            _contents = contents;

            if (_contents is INotifyCollectionChanged notifyingContents)
                notifyingContents.CollectionChanged += _notifyCollectionChanged;
        }

        public void Add(T item)
        {
            _contents.Add(item);
            _notifyCollectionChanged(NotifyCollectionChangedAction.Add, item, _contents.Count - 1);
        }
        public void Insert(Int32 index, T item)
        {
            _contents.Insert(index, item);
            _notifyCollectionChanged(NotifyCollectionChangedAction.Add, item, index);
        }
        public void RemoveAt(Int32 index)
        {
            T item = _contents[index];

            _contents.RemoveAt(index);
            _notifyCollectionChanged(NotifyCollectionChangedAction.Remove, item, index);
        }
        public Boolean Remove(T item)
        {
            Int32 index = _contents.IndexOf(item);
            Boolean result = _contents.Remove(item);

            _notifyCollectionChanged(NotifyCollectionChangedAction.Remove, item, index);
            return result;
        }
        public void Clear()
        {
            _contents.Clear();
            _notifyCollectionChanged(NotifyCollectionChangedAction.Reset);
        }

        public Int32 IndexOf(T item) => _contents.IndexOf(item);
        public Boolean Contains(T item) => _contents.Contains(item);
        public void CopyTo(T[] array, Int32 arrayIndex) => _contents.CopyTo(array, arrayIndex);

        private void _notifyCollectionChanged(NotifyCollectionChangedAction action) => CollectionChanged!(this, new NotifyCollectionChangedEventArgs(action));
        private void _notifyCollectionChanged(NotifyCollectionChangedAction action, Object? value, Int32 index = -1) => CollectionChanged!(this, new NotifyCollectionChangedEventArgs(action, value, index));
        private void _notifyCollectionChanged(NotifyCollectionChangedAction action, Object? newValue, Object? oldValue, Int32 index = -1) => CollectionChanged!(this, new NotifyCollectionChangedEventArgs(action, newValue, oldValue, index));
        private void _notifyCollectionChanged(Object? source, NotifyCollectionChangedEventArgs args) => CollectionChanged!(this, args);

        public IEnumerator<T> GetEnumerator() => _contents.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

        public void Refresh() => _notifyCollectionChanged(NotifyCollectionChangedAction.Reset);

        private Boolean _disposed = false;
        public void Dispose()
        {
            if (_disposed) return;
            if (_contents is INotifyCollectionChanged notifyingContents)
                notifyingContents.CollectionChanged += _notifyCollectionChanged;

            _disposed = true;
        }
    }
}
