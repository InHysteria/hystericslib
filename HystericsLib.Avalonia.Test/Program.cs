global using System;
global using System.Linq;
global using System.Collections.Generic;
global using System.Collections.ObjectModel;

global using HystericsLib.Avalonia;

using Autofac;
using Avalonia;
using Avalonia.Logging;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Rendering;

ContainerBuilder builder = new ContainerBuilder();

builder.RegisterModule(new HystericsLib.Avalonia.Module(DarkTheme: true));

builder.RegisterType<Avalonia.Themes.Simple.SimpleTheme>().As<Avalonia.Styling.IStyle>();

builder.RegisterType<HystericsLib.Avalonia.Test.UI.Themes.Default>().AsSelf().As<Avalonia.Styling.IStyle>();
builder.RegisterType<HystericsLib.Avalonia.Test.UI.MainViewModel>().As<HystericsLib.Avalonia.IMainViewModel>();
//builder.RegisterType<HystericsLib.Avalonia.Test.UI.AViewModel>().AsSelf();
//..

using (IContainer container = builder.Build())
using (ILifetimeScope scope = container.BeginLifetimeScope())
	scope.Resolve<AppBuilder>()
		.UsePlatformDetect()
		.LogToTrace()
		.StartWithClassicDesktopLifetime(args);
