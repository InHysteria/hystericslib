using HystericsLib.Avalonia.Controls;

namespace HystericsLib.Avalonia.Test.UI
{
    [ResolvesTo(typeof(MainView))]
    public class MainViewModel : ViewModel, IMainViewModel
    {
        public String Title { get; } = "HystericsLib.Avalonia.Test";
		
		public Int32 Width { get; } = 1080;
		public Int32 Height { get; } = 720;
		
		
		public MenuViewModel Menu { get; }
		public TreeContentViewModel TreeContent { get; }

		public LambdaCommand MenuTestCommand { get; }
		public LambdaCommand AddToTreeCommand { get; }
		
		private Boolean _toggle;
		public Boolean Toggle
		{
			get => _toggle;
			set 
			{
				if (Set(ref _toggle, value))
					Console.WriteLine("Toggled: " + Toggle);
			}
		}
		
		public MainViewModel(MenuViewModel menu, TreeContentViewModel treeContent)
		{
			MenuTestCommand = new LambdaCommand(_ => Console.WriteLine("TEST!"));
			AddToTreeCommand = new LambdaCommand(_ => 		
			{
				TestTreeNodeViewModel newNode = new(Guid.NewGuid().ToString().Substring(6));
				ITreeNode target = treeContent.Selected ?? treeContent;

				newNode.Parent = target;
				target.Children.Add(newNode);
			});
			
			Menu = menu
				.Start()
					.StartMenu("MenuTest")
						.AddEntry("Command", MenuTestCommand)
						.AddEntry("Add to tree", AddToTreeCommand)
						.AddSeparator()
						.AddToggle("ToggleTest", () => _toggle, v => _toggle = v)
					.EndMenu()
				.End();

			TreeContent = new()
			{
				Children = new ObservableCollection<ITreeNode>()
				{
					new TestTreeNodeViewModel("Example A")
					{
						Children = new ObservableCollection<ITreeNode>()
						{
							new TestTreeNodeViewModel("Example A.1")
							{
								Children = new ObservableCollection<ITreeNode>()
								{							
									new TestTreeNodeViewModel("Example A.1.i"),
									new TestTreeNodeViewModel("Example A.1.ii"),
									new TestTreeNodeViewModel("Example A.1.iii")
								}
							},
							new TestTreeNodeViewModel("Example A.2")
							{
								Children = new ObservableCollection<ITreeNode>()
								{							
									new TestTreeNodeViewModel("Example A.2.i"),
									new TestTreeNodeViewModel("Example A.2.ii"),
									new TestTreeNodeViewModel("Example A.2.iii"),
									new TestTreeNodeViewModel("Example A.2.iv")
								}
							},
							new TestTreeNodeViewModel("Example A.3")
						}
					},
					new TestTreeNodeViewModel("Example B")
					{
						Children = new ObservableCollection<ITreeNode>()
						{
							new TestTreeNodeViewModel("Example B.1")
							{
								Children = new ObservableCollection<ITreeNode>()
								{							
									new TestTreeNodeViewModel("Example B.1.i"),
									new TestTreeNodeViewModel("Example B.1.ii"),
									new TestTreeNodeViewModel("Example B.1.iii")
								}
							},
							new TestTreeNodeViewModel("Example B.2")
							{
								Children = new ObservableCollection<ITreeNode>()
								{							
									new TestTreeNodeViewModel("Example B.2.i"),
									new TestTreeNodeViewModel("Example B.2.ii"),
									new TestTreeNodeViewModel("Example B.2.iii"),
									new TestTreeNodeViewModel("Example B.2.iv")
								}
							},
							new TestTreeNodeViewModel("Example B.3")
						}
					},
					new TestTreeNodeViewModel("Example C")
				}
			};

			Setup(TreeContent);
		}
		
		private ITreeNode Setup(ITreeNode target)
		{
			foreach (ITreeNode child in target.Children)
				Setup(child).Parent = target;

			return target;
		}
    }
}
