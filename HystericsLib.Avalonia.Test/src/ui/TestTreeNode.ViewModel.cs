using HystericsLib.Avalonia.Controls;

namespace HystericsLib.Avalonia.Test.UI
{
    [ResolvesTo(typeof(TestTreeNodeHeader), typeof(TestTreeNodeView))]
    public class TestTreeNodeViewModel(String Name) : ViewModel, ITreeNode
    {
		private String _name = Name;
		public String Name
		{
			get => _name;
			set => Set(ref _name, value);
		}

		public ITreeNode? Parent { get; set; }
		public IList<ITreeNode> Children { get; init; } = new ObservableCollection<ITreeNode>();
    }
}
