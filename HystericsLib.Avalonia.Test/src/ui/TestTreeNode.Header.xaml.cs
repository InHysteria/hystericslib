using Avalonia;
using Avalonia.Input;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace HystericsLib.Avalonia.Test.UI
{
    public class TestTreeNodeHeader : UserControl
    {
        public TestTreeNodeHeader()
        {
            this.InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
