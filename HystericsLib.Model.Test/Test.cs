namespace HystericsLib.Model
{
    public class Character
    {
        public static Definition<String> NameProperty { get; } = new Definition<String>("Name");

        private Container _container = new();

        public String Name
        {
            get => _container.Get(NameProperty) ?? "";
            set => _container.Set(NameProperty, value);
        }
    }
}
