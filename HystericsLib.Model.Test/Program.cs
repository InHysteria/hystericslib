global using System;
global using System.Linq;
global using System.Collections.Generic;
global using System.Collections.ObjectModel;

global using HystericsLib.Model;

namespace HystericsLib.Model.Test
{
    public static class Program
    {
        public static void Main()
        {
            Character c = new();
            c.Name = "Steve";

            Console.WriteLine($"Character..");
            Console.WriteLine($"    Name: {c.Name}");
            Console.WriteLine($"    NameOverride: {c.NameOverride}");
            Console.WriteLine($"    Strength: {c.Strength}");
        }
    }

    public class Character : Container
    {
        public static Definition<String> NameProperty { get; } = new Definition<String>("Name");
        public static Definition<String> NameOverrideProperty { get; } = new Definition<String>("NameOverride");
        public static Definition<Int32> StrengthProperty { get; } = new Definition<Int32>("Strength");

        public String Name
        {
            get => Get(NameProperty) ?? "";
            set => Set(NameProperty, value);
        }

        public String NameOverride
        {
            get => Get(NameOverrideProperty) ?? "";
            set => Set(NameOverrideProperty, value);
        }

        public Int32 Strength
        {
            get => Get(StrengthProperty);
            set => Set(StrengthProperty, value);
        }
    }
}
